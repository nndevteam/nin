
[Setup]
AppName=DrunkGodfather's NIN
AppVersion=1.0.0
WizardStyle=modern
DefaultDirName={sd}\games\DrunkGodfather's NIN
DefaultGroupName=DrunkGodfather's NIN
UninstallDisplayIcon={app}\NIN.exe
Compression=lzma2
SolidCompression=yes
OutputDir=D:\

DisableWelcomePage=no
PrivilegesRequired=lowest
DisableDirPage=no
DisableProgramGroupPage=no

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; \
    GroupDescription: "{cm:AdditionalIcons}";

[Files]
Source: "D:\_CODING\NONAMEDEV\UNITY\DrawingSomething\RELEASE\GAME\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs

[Icons]
Name: "{group}\DrunkGodfather's NIN"; Filename: "{app}\NIN.exe"
Name: "{userdesktop}\DrunkGodfather's NIN"; Filename: "{app}\NIN.exe"; \
    Tasks: desktopicon