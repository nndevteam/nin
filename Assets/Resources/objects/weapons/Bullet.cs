﻿using System.Collections;
using UnityEngine;

public class Bullet : MonoBehaviour {
    public float baseDamage = 10;
    public float additionalDamage = 0;
    public float speed = 100;
    public int countPenetration = 1;

    private GameObject self;
    private string targetLine = FireArms.CENTER_LAYER;
    private float accuracy = 10.0f;

    //Prevent multi hit
    private bool isHited = false;
    //BulletTopPhisics
    //BulletCenterPhisics
    //BulletBottomPhisics

    void Start() {
        GetComponent<Rigidbody2D>().AddForce(transform.right * speed * GetComponent<Rigidbody2D>().mass * 1000);
        StartCoroutine(setLayer());
        Destroy(gameObject, 3);
    }

    void Update() {
        //transform.position += transform.right * speed * Time.deltaTime * 17; 
    }

    public void bind(GameObject self, string line, float _accuracy, float _additionalDamage) {
        this.targetLine = line;
        this.accuracy = _accuracy;
        this.additionalDamage = _additionalDamage;
        this.self = self;
        AI AI = self.GetComponent<CharecterController>().getAI();
        if (AI != null) {
            foreach (GameObject viewObj in AI.getGroupInView()) if (viewObj.GetComponent<CharecterHitControler>() != null) {
                Physics2D.IgnoreCollision(GetComponent<BoxCollider2D>(), viewObj.GetComponent<CharecterHitControler>().head);
                Physics2D.IgnoreCollision(GetComponent<BoxCollider2D>(), viewObj.GetComponent<CharecterHitControler>().torso);
                Physics2D.IgnoreCollision(GetComponent<BoxCollider2D>(), viewObj.GetComponent<CharecterHitControler>().bottom);
            }
        }
    }

    void OnCollisionEnter2D(Collision2D coll) {
        if (isHited) return;
        
        if (coll.gameObject.GetComponent<HitController>() != null) {
            isHited = true;
            coll.gameObject.GetComponent<HitController>().setHit(coll.contacts[0].point, baseDamage + additionalDamage);
            if (coll.gameObject.GetComponent<HitController>().getEffect() != null) {
                GameObject effect = Instantiate(
                    coll.gameObject.GetComponent<HitController>().getEffect(),
                    new Vector3(coll.contacts[0].point.x, coll.contacts[0].point.y, 0),
                    transform.rotation
                );
                effect.GetComponent<ChildLayers>().setLayer(
                    coll.gameObject.GetComponent<HitController>().layerName,
                    coll.gameObject.GetComponent<HitController>().layerSortingOrder
                );
            }
        }
        if (coll.gameObject.GetComponent<CharecterHitControler>() != null) {
            isHited = true;
            if (coll.gameObject.GetComponent<CharecterController>().getAI() != null) {
                coll.gameObject.GetComponent<CharecterController>().getAI().setTarget(self);
            }
            string fxType = coll.gameObject.GetComponent<CharecterHitControler>().setHit(coll, baseDamage + additionalDamage);
            if (coll.gameObject.GetComponent<CharecterHitControler>().getEffect(fxType) != null) {
                GameObject effect = Instantiate(
                    coll.gameObject.GetComponent<CharecterHitControler>().getEffect(fxType),
                    new Vector3(coll.contacts[0].point.x, coll.contacts[0].point.y, 0),
                    transform.rotation
                );
                effect.GetComponent<ChildLayers>().setLayer(
                    "Charecter",
                    coll.gameObject.GetComponent<CharecterLayers>().getStartLayer() + 9 //BASE HEAD 9
                );
            }
        }
        
        Destroy(gameObject);
    }

    IEnumerator setLayer() {
        yield return new WaitForSeconds(0.3f);

        ArrayList listLines = new ArrayList();
        listLines.Add(FireArms.TOP_LAYER);
        listLines.Add(FireArms.CENTER_LAYER);
        listLines.Add(FireArms.BOTTOM_LAYER);
        listLines.Remove(targetLine);
        float chanceCurrentLine = 100 - (accuracy * 5);
        int chance = Random.Range(0, 101);
        if (chanceCurrentLine > chance) {
            gameObject.layer = LayerMask.NameToLayer(targetLine);
        } else {
            int chanceOther = Random.Range(0, 101);
            if (chanceOther < 51) {
                gameObject.layer = LayerMask.NameToLayer(listLines[0].ToString());
            } else {
                gameObject.layer = LayerMask.NameToLayer(listLines[1].ToString());
            }
        }

        yield return new WaitForSeconds(0.4f);
        Destroy(GetComponent<BoxCollider2D>());
        baseDamage = 0;
        additionalDamage = 0;
    }
}
