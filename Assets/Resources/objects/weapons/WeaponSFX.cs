﻿using UnityEngine;

public class WeaponSFX : MonoBehaviour {
    public AudioClip reloadSFX;
    
    public void onReload() {
        if (SavedSettings.getInstance().isSound() && reloadSFX != null) {
            GetComponent<AudioSource>().clip = reloadSFX;
            GetComponent<AudioSource>().Play();
        }
    }
}
