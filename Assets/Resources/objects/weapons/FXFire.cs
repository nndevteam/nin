﻿using UnityEngine;

public class FXFire : MonoBehaviour {
    public Sprite[] fx;
    
    void Start() {
        if (fx.Length != 0)
            GetComponent<SpriteRenderer>().sprite = fx[Random.Range(0, fx.Length)];
    }

    public void show() {
        if (fx.Length != 0)
            GetComponent<SpriteRenderer>().sprite = fx[Random.Range(0, fx.Length)];

        gameObject.SetActive(true);
    }
}
