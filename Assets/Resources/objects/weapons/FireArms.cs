﻿using System.Collections;
using UnityEngine;

public class FireArms : MonoBehaviour {
    public const string TOP_LAYER = "BulletTopPhisics";
    public const string CENTER_LAYER = "BulletCenterPhisics";
    public const string BOTTOM_LAYER = "BulletBottomPhisics";
    public string baseName = "Unknown";
    public string namePostFix = "";
    public int hands = 1;
    public GameObject bullet;
    public GameObject view;
    public GameObject barrel;
    public GameObject fx;

    public bool automatic = false;
    public float accuracy = 10.0f;
    public float additionalDamage = 0.0f;
    public float animRate = 0.01f;
    public float fireRate = 0.5f;
    public int ammoCount = 90;
    public int ammoClipSize = 14;
    public int ammoClip = 0;
    private float lastPressing = 0;
    private float lastIteract = 0;

    private GameObject user;
    private string targetLine = CENTER_LAYER;
    private float reloadTime = 1f;

    // Start is called before the first frame update
    void Start() {
        ammoClip = ammoClipSize;
    }

    public void setLine(string line) {
        targetLine = line;
    }

    public void onGetWeapon(GameObject user) {
        if (user != null) {
            user.GetComponent<CharecterController>().replaceWeapon(gameObject);
        }
    }

    public void onLoseWeapon(Transform world) {
        transform.parent = world;
        user = null;
        ammoCount = 90;
        StartCoroutine(onLoseWeaponBindAction());
    }

    public void setUser(GameObject obj) {
        user = obj;
        setLayer(obj.GetComponent<CharecterLayers>().getStartLayer() + 15);
    }

    public void setLayer(int layer) {
        view.GetComponent<SpriteRenderer>().sortingLayerName = "Charecter";
        view.GetComponent<SpriteRenderer>().sortingOrder = layer;
        fx.GetComponent<SpriteRenderer>().sortingOrder = layer + 1;
    }

    void FixedUpdate() {
        if (fx != null) if (lastIteract + animRate < Time.time) 
                fx.SetActive(false);

        if (user != null && user.GetComponent<CharecterController>() != null) user.GetComponent<CharecterController>().doAction("isShot", false);
    }

    public void iteract(float pressing, GameObject self) {
        if (pressing == 0) {
            lastPressing = 0;
            return;
        }
        //if (!automatic && lastPressing > 0) return;
        lastPressing = pressing;

        if (lastIteract + fireRate > Time.time) return;
        lastIteract = Time.time;

        if (ammoClip == 0) {
            if (ammoCount == 0) return;
            if (ammoCount > ammoClipSize) {
                ammoClip = ammoClipSize;
                ammoCount -= ammoClipSize;
            } else {
                ammoClip = ammoCount;
                ammoCount = 0;
            }
            GetComponent<WeaponSFX>().onReload();
            lastIteract = Time.time + reloadTime;
            if (user != null) user.GetComponent<CharecterController>().doAction("reload");
            return;
        }

        if (fx != null) fx.GetComponent<FXFire>().show();
        if (user != null) user.GetComponent<CharecterController>().doAction("isShot", true);

        Quaternion randRotation = barrel.transform.rotation;
        Vector3 euler = randRotation.eulerAngles;

        float centerEuler = 0f;
        switch (targetLine) {
            case TOP_LAYER:
                centerEuler = 3f;
                break;
            case CENTER_LAYER:
                centerEuler = 0f;
                break;
            case BOTTOM_LAYER:
                centerEuler = -7f;
                break;
            default:
                centerEuler = 0f;
                break;
        }

        GetComponent<SFXRandom>().onSound();
        euler.z = Random.Range(centerEuler - accuracy, centerEuler + accuracy);
        randRotation.eulerAngles = euler;

        GameObject b = Instantiate(
             bullet,
             barrel.transform.position,
             randRotation
        );
        b.GetComponent<Bullet>().bind(self, targetLine, accuracy, additionalDamage);
        ammoClip--;
    }

    private IEnumerator onLoseWeaponBindAction() {
        ResourceRequest resourcesRequest = Resources.LoadAsync("locations/action_pickup", typeof(GameObject));

        while (!resourcesRequest.isDone)
            yield return null;

        GameObject actionObj = resourcesRequest.asset as GameObject;
        GameObject action = Instantiate(
             actionObj,
             new Vector3(this.transform.position.x, this.transform.position.y, 0),
             this.transform.parent.rotation,
             this.transform.parent
        );
        action.GetComponent<ActionController>().iteractAction = this.gameObject;
    }
}
