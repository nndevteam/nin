﻿using UnityEngine;

public class HitController : MonoBehaviour {
    public bool isAutoLayer = false;
    public GameObject[] fx;

    public string layerName = "Default";
    public int layerSortingOrder = 0;

    void Start() {
        if (this.isAutoLayer && GetComponent<SpriteRenderer>() != null) {
            this.layerName = GetComponent<SpriteRenderer>().sortingLayerName;
            this.layerSortingOrder = GetComponent<SpriteRenderer>().sortingOrder + 1;
        }
    }

    public void setHit(Vector2 point, float damage) {
        
    }

    
    public GameObject getEffect() {
        GameObject _fx = null;
        if (fx != null && fx.Length > 0) {
            _fx = fx[Random.Range(0, fx.Length)];
        }
        return _fx;
    }
}
