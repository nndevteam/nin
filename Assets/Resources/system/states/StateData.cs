﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

[Serializable]
public class StateData {
    public static StateData instance = null;
    private static string saveName = Application.persistentDataPath + "/state.nnd";

    private bool _isSaveExists = false;
    private string completion = "";

    private int floorDifficult = 1;
    private PairValue floorBind = new PairValue(15, 11);
    private bool _isShowControlHint = true;

    public static StateData getInstance() {
        if (instance == null) {
            if (File.Exists(saveName)) {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                FileStream file = File.Open(saveName, FileMode.Open);
                instance = (StateData)binaryFormatter.Deserialize(file);
                file.Close();
            } else {
                instance = new StateData();
            }
        }
        return instance;
    }

    public StateData() {
        
    }

    public static void reset() {
        instance = new StateData();
        instance.save();
    }

    public bool isSaveExists() {
        return _isSaveExists;
    }

    private void save() {
        _isSaveExists = true;

        BinaryFormatter binaryFormatter = new BinaryFormatter();
        FileStream file = File.Create(saveName);
        binaryFormatter.Serialize(file, instance);
        file.Close();
    }

    public bool isComplete(string key) {
        return completion.Contains("|" + key + "|");
    }

    public void setComplete(string key) {
        completion += "|" + key + "|";
        save();
    }

    public int getFloorDifficult() {
        return floorDifficult;
    }

    public void setFloorDifficult(int val) {
        if (val > floorDifficult) floorDifficult = val;
        save();
    }

    public void upFloorDifficult() {
        floorDifficult++;
        save();
    }

    public PairValue getFloorBindVals() {
        return floorBind;
    }
    public void setFloorBindVals(PairValue val) {
        floorBind = val;
        save();
    }

    public void setHideControlHint() {
        _isShowControlHint = false;
        save();
    }

    public bool isShowControlHint() {
        return _isShowControlHint;
    }
}
