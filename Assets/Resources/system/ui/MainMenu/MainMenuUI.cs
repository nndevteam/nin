﻿using UnityEngine;
using UnityEngine.UI;

public class MainMenuUI : MonoBehaviour {
    public GameObject soundTgl;
    public GameObject musicTgl;
    public GameObject bloodTgl;

    public GameObject menuMusic;

    void Start() {
        soundTgl.GetComponent<Toggle>().isOn = SavedSettings.getInstance().isSound();
        musicTgl.GetComponent<Toggle>().isOn = SavedSettings.getInstance().isMusic();
        bloodTgl.GetComponent<Toggle>().isOn = SavedSettings.getInstance().isBloody();
    }

    public void onClearSave() {
        StateData.reset();
        //StateData.getInstance().setFloorDifficult(15);
    }

    public void onStart() {
        GetComponent<SceneLoader>().launchById("menu_hub");
    }

    public void onQuit() {
        Application.Quit();
    }

    public void onSoundToogle(bool val) {
        SavedSettings.getInstance().setSound(val);
    }
    public void onMusicToogle(bool val) {
        SavedSettings.getInstance().setMusic(val);
        if (val) {
            menuMusic.GetComponent<AudioSource>().mute = false;
            menuMusic.GetComponent<AudioSource>().Play();
        } else {
            menuMusic.GetComponent<AudioSource>().mute = true;
            menuMusic.GetComponent<AudioSource>().Stop();
        }
    }

    public void onBloodToogle(bool val) {
        SavedSettings.getInstance().setBloody(val);
    }

}