﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformSelect : MonoBehaviour {
    public bool forPC = false;
    public bool forMobile = false;
    // Start is called before the first frame update
    void Start() {
        if (Constants.isMobile) {
            if (forMobile) {
                gameObject.SetActive(true);
            } else {
                gameObject.SetActive(false);
            }
        } else {
            if (forPC) {
                gameObject.SetActive(true);
            } else {
                gameObject.SetActive(false);
            }
        }
    }
}
