﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionMenu : MonoBehaviour {
    public GameObject pauseMenu;
    public GameObject failMissionMenu;
    public GameObject winMissionMenu;

    public GameObject controlHint;

    void Start() {
        pauseMenu.SetActive(false);
        failMissionMenu.SetActive(false);
        winMissionMenu.SetActive(false);
        controlHint.SetActive(false);
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) { 
            if (pauseMenu.activeSelf) {
                pauseMenu.SetActive(false);
                Time.timeScale = 1;
            } else if (!failMissionMenu.activeSelf && !winMissionMenu.activeSelf) {
                pauseMenu.SetActive(true);
                Time.timeScale = 0;
            }
        }
    }

    public void onPause() {
        pauseMenu.SetActive(true);
        Time.timeScale = 0;
    }

    public void onContinue() {
        pauseMenu.SetActive(false);
        Time.timeScale = 1;
    }

    public void onControlHint() {
        pauseMenu.SetActive(false);
        controlHint.SetActive(true);
    }

    public void onHideControlHint(bool _isShowNextTime) {
        controlHint.SetActive(false);
        Time.timeScale = 1;
        if (! _isShowNextTime) {
            StateData.getInstance().setHideControlHint();
        }
    }

    public void onExit() {
        Time.timeScale = 1;
        GetComponent<SceneLoader>().launchById("menu_hub");
    }

    public IEnumerator onWin() {
        yield return new WaitForSeconds(3f);
        pauseMenu.SetActive(false);
        winMissionMenu.SetActive(true);
    }

    public IEnumerator onFail() {
        yield return new WaitForSeconds(3f);
        pauseMenu.SetActive(false);
        failMissionMenu.SetActive(true);
    }
}
