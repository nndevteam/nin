﻿using UnityEngine;
using UnityEngine.UI;

public class CardView : MonoBehaviour {
    private RandomLocationOptions randomLocationOptions;
    public GameObject difficultView;
    public GameObject heightView;

    public GameObject levelUp;
    public GameObject levelDown;

    public void setRandomLocationOptions(RandomLocationOptions _randomLocationOptions, int levelChange) {
        this.randomLocationOptions = _randomLocationOptions;
        if (difficultView != null) difficultView.GetComponent<Text>().text = this.randomLocationOptions.difficult.ToString();
        if (heightView != null) heightView.GetComponent<Text>().text = ((this.randomLocationOptions.blockCount + 2) * 2).ToString();

        if (levelChange == 0) {
            levelUp.SetActive(false);
            levelDown.SetActive(false);
        } else if (levelChange > 0) {
            levelUp.SetActive(true);
            levelDown.SetActive(false);
        } else {
            levelUp.SetActive(false);
            levelDown.SetActive(true);
        }
    }

    public void bindMaxCard() {
        PairValue defs = StateData.getInstance().getFloorBindVals();
        if (difficultView != null) difficultView.GetComponent<Text>().text = defs.getVal1().ToString();
        if (heightView != null) heightView.GetComponent<Text>().text = ((defs.getVal2() + 2) * 2).ToString();
    }

    public void setDifficult(int change) {
        PairValue defs = StateData.getInstance().getFloorBindVals();
        int newDifficult = defs.getVal1() + change;
        if (newDifficult < 1) newDifficult = 1;
        StateData.getInstance().setFloorBindVals(new PairValue(newDifficult, defs.getVal2()));
        bindMaxCard();
    }

    public void setHeight(int change) {
        PairValue defs = StateData.getInstance().getFloorBindVals();
        int newHeight = defs.getVal2() + change;
        if (newHeight < 1) newHeight = 1;
        StateData.getInstance().setFloorBindVals(new PairValue(defs.getVal1(), newHeight));
        bindMaxCard();
    }

}
