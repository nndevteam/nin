﻿
using System;

[Serializable]
public class PairValue {
    private int v1;
    private int v2;

    public PairValue(int v1, int v2) {
        this.v1 = v1;
        this.v2 = v2;
    }

    public int getVal1() {
        return v1;
    }
    public int getVal2() {
        return v2;
    }
}
