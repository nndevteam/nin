﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionHubMenu : MonoBehaviour {
    public GameObject floorZeroCard;
    public GameObject floorPrevCard;
    public GameObject floorCurrentCard;
    public GameObject floorNextCard;
    public GameObject floorMaxCard;

    public RandomLocationOptions randomLocationOptionsPrev;
    public RandomLocationOptions randomLocationOptionsCurrent;
    public RandomLocationOptions randomLocationOptionsNext;

    void Start() {
        //GEN FLOOR MISSION
        //StateData.reset();
        if (StateData.getInstance().getFloorDifficult() == 1) {
            floorZeroCard.SetActive(true);
            floorPrevCard.SetActive(false);
        } else {
            floorZeroCard.SetActive(false);
            floorPrevCard.SetActive(true);
        }
        int currentFloorDiff = StateData.getInstance().getFloorDifficult();
        if (StateData.getInstance().getFloorDifficult() >= 15) {
            floorMaxCard.SetActive(true);
            floorNextCard.SetActive(false);
        } else {
            floorMaxCard.SetActive(false);
            floorNextCard.SetActive(true);
        }
        floorMaxCard.GetComponent<CardView>().bindMaxCard();
        //Prev
        int prevFloorDiff = currentFloorDiff - 1;
        randomLocationOptionsPrev = new RandomLocationOptions(
                new OldGangLocation()
        );
        randomLocationOptionsPrev.blockCount = Random.Range(prevFloorDiff / 2, prevFloorDiff);
        randomLocationOptionsPrev.difficult = prevFloorDiff;
        floorPrevCard.GetComponent<CardView>().setRandomLocationOptions(randomLocationOptionsPrev, -1);
        //Current
        randomLocationOptionsCurrent = new RandomLocationOptions(
                new OldGangLocation()
        );
        randomLocationOptionsCurrent.blockCount = Random.Range((int)(currentFloorDiff / 1.75f), currentFloorDiff + 1);
        randomLocationOptionsCurrent.difficult = currentFloorDiff;
        floorCurrentCard.GetComponent<CardView>().setRandomLocationOptions(randomLocationOptionsCurrent, 0);
        //Next
        int nextFloorDiff = currentFloorDiff + 1;
        randomLocationOptionsNext = new RandomLocationOptions(
                new OldGangLocation()
        );
        randomLocationOptionsNext.blockCount = Random.Range((int)(nextFloorDiff / 1.5f), nextFloorDiff + 2);
        randomLocationOptionsNext.difficult = nextFloorDiff;
        floorNextCard.GetComponent<CardView>().setRandomLocationOptions(randomLocationOptionsNext, 1);

    }

    public void setDifficult(int change){
        floorMaxCard.GetComponent<CardView>().setDifficult(change);
    }

    public void setHeight(int change) {
        floorMaxCard.GetComponent<CardView>().setHeight(change);
    }

    public void onLaunchFloorMission(int levelChange) {
        if (levelChange == 0) {
            SessionData.getInstance().setRandomLocationOptions(randomLocationOptionsCurrent);
        } else if (levelChange > 0) {
            SessionData.getInstance().setRandomLocationOptions(randomLocationOptionsNext);
        } else {
            SessionData.getInstance().setRandomLocationOptions(randomLocationOptionsPrev);
        }
        GetComponent<SceneLoader>().launchById("game_floors");
    }

    public void onLaunchFloorMissionCustom() {
        PairValue defs = StateData.getInstance().getFloorBindVals();
        RandomLocationOptions randomLocationOptions = new RandomLocationOptions(
                new OldGangLocation()
        );
        randomLocationOptions.difficult = defs.getVal1();
        randomLocationOptions.blockCount = defs.getVal2();
        
        SessionData.getInstance().setRandomLocationOptions(randomLocationOptions);
        
        GetComponent<SceneLoader>().launchById("game_floors");
    }

    public void onQuit() {
        Application.Quit();
    }
}
