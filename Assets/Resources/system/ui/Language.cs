﻿using System.Collections.Generic;
using UnityEngine;

public class Language {
    public static Language instance = null;

    private Dictionary<string, string> translationList = new Dictionary<string, string>();

    public static Language getInstance() {
        if (instance == null) {
            if (Application.systemLanguage == SystemLanguage.Russian) {
                return init("ru");
            }
            if (Application.systemLanguage == SystemLanguage.Chinese || Application.systemLanguage == SystemLanguage.ChineseSimplified || Application.systemLanguage == SystemLanguage.ChineseTraditional) {
                return init("ch");
            }
            return init("en");
        }
        return instance;
    }

    public static Language init(string langCode) {
        //langCode = "en";
        instance = new Language(langCode);
        return instance;
    }

    public Language(string langCode) {
        if (langCode == "ru") {
            translationList = new RU().getTranslations();
        } else {
            translationList = new EN().getTranslations();
        }
    }

    public string get(string stringId) {
        if (translationList.ContainsKey(stringId)) {
            return translationList[stringId];
        } else {
            return stringId;
        }
    }

}