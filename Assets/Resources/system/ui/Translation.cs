﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Translation : MonoBehaviour
{
    public string stringId = "default";

    void Start() {
        if (GetComponent<Text>() != null) GetComponent<Text>().text = Language.getInstance().get(stringId);
        if (GetComponent<TextMeshPro>() != null) GetComponent<TextMeshPro>().text = Language.getInstance().get(stringId);
    }

}
