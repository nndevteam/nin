﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TranslationVariants : MonoBehaviour {
    public string[] stringIds;

    void Start() {
        if (stringIds == null && stringIds.Length == 0) return;
        string text = stringIds[Random.Range(0, stringIds.Length)];
        if (GetComponent<Text>() != null) GetComponent<Text>().text = Language.getInstance().get(text);
        if (GetComponent<TextMeshPro>() != null) GetComponent<TextMeshPro>().text = Language.getInstance().get(text);
    }
}
