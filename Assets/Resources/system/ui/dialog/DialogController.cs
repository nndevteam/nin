﻿using System.Collections;
using TMPro;
using UnityEngine;

public class DialogController : MonoBehaviour {
    private GameObject parent;
    private string[] dialogs;
    private float[] timeToRead;


    public GameObject textView;

    public void setParent(GameObject _parent, Vector3 position) {
        parent = _parent;
        transform.position = position;
    }

    public void setText(string[] text, float[] timeToRead) {
        this.dialogs = text;
        this.timeToRead = timeToRead;
        
        StartCoroutine(onNext());
    }

    public void onClose() {
        Destroy(gameObject);
    }

    void Update() {
        if (parent == null) return;
        transform.position = new Vector3(parent.transform.position.x, transform.position.y, 0);
    }

    private IEnumerator onNext() {
        for (int i = 0; i < dialogs.Length; i++) {
            textView.GetComponent<TextMeshPro>().text = Language.getInstance().get(dialogs[i]);
            yield return new WaitForSeconds(timeToRead[i]);
        }

        onClose();
    }
}
