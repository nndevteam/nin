﻿using System.Collections;
using UnityEngine;

public class LoadProgressAnimation : MonoBehaviour {
    private float time = 0;
    void Start() {
        StartCoroutine(animate());
    }

    IEnumerator animate() {
        while (true) {
            if (time < Time.unscaledTime) {
                transform.rotation = Random.rotation;
                time = Time.unscaledTime + 0.2f;
            }
            yield return null;
        }
    }
}
