﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class UIAmmoCount : MonoBehaviour {
    public GameObject view;
    public GameObject ammoClipView;
    public GameObject ammoCountView;

    public void setUIVisibility(bool isVisible) {
        view.SetActive(isVisible);
    }

    void LateUpdate() {
        try {
            view.SetActive(true);
            int ammoClip = GameObject.FindGameObjectWithTag("Global")
                .GetComponent<PlayerController>()
                .getPlayerObj().GetComponent<CharecterController>()
                .getActiveWeapon().GetComponent<FireArms>().ammoClip;

            int ammoCount = GameObject.FindGameObjectWithTag("Global")
                .GetComponent<PlayerController>()
                .getPlayerObj().GetComponent<CharecterController>()
                .getActiveWeapon().GetComponent<FireArms>().ammoCount;

            ammoClipView.GetComponent<Text>().text = ammoClip + "";
            ammoCountView.GetComponent<Text>().text = ammoCount + "";
        } catch (Exception ex) {
            view.SetActive(false);
        }
    }
}
