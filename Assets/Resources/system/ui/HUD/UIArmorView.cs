﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class UIArmorView : MonoBehaviour {
    public GameObject view;

    void LateUpdate() {
        try {
            float procentArmor = GameObject.FindGameObjectWithTag("Global")
                .GetComponent<PlayerController>()
                .getPlayerObj().GetComponent<CharecterStats>().getProcentArmor();
            view.GetComponent<Image>().fillAmount = procentArmor;
        } catch (Exception ex) {
            view.GetComponent<Image>().fillAmount = 0;
        }
    }
}
