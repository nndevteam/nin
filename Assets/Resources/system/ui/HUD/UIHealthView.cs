﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class UIHealthView : MonoBehaviour {
    public GameObject view;

    void LateUpdate() {
        try {
            float procentHealth = GameObject.FindGameObjectWithTag("Global")
                .GetComponent<PlayerController>()
                .getPlayerObj().GetComponent<CharecterStats>().getProcentHealth();
            view.GetComponent<Image>().fillAmount = procentHealth;
        } catch (Exception ex) {
            view.GetComponent<Image>().fillAmount = 0;
        }
    }
}
