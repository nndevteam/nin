﻿using System.Collections;
using UnityEngine;

public class SectionController : MonoBehaviour {
    public string _types = "outdoor|boxes|living|technical";
    public int _variantX = 123;

    void Start() {
        string[] types;
        if (_types.Contains("|")) {
            types = _types.Split('|');
        } else {
            types = new string[1];
            types[0] = _types;
        }

        StartCoroutine(LoadSection(
            types[Random.Range(0, types.Length)],
            _variantX
        ));
    }

    private IEnumerator LoadSection(string type, int variant) {
        int maxVarinats = 1;
        if (type.Equals("outdoor")) {
            maxVarinats = SectionOutdoor.getVarinatsByX(variant);
        }
        if (type.Equals("boxes")) {
            maxVarinats = SectionBoxes.getVarinatsByX(variant);
        }
        if (type.Equals("living")) {
            maxVarinats = SectionLiving.getVarinatsByX(variant);
        }
        if (type.Equals("technical")) {
            maxVarinats = SectionTechnical.getVarinatsByX(variant);
        }

        maxVarinats++;
        string sectionId = "locations/packs/sections/" + type + "/x" + variant + "_" + Random.Range(1, maxVarinats);
        L.S("SectionController:LoadSection: " + sectionId);
        ResourceRequest resourcesRequest = Resources.LoadAsync(sectionId, typeof(GameObject));
        while (!resourcesRequest.isDone)
            yield return null;

        GameObject sectionObj = resourcesRequest.asset as GameObject;
        GameObject section = Instantiate(
             sectionObj,
             this.gameObject.transform.position,
             this.gameObject.transform.rotation,
             this.gameObject.transform.parent
        );
        Destroy(this.gameObject);
    }
}
