﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AISectionDoorController : MonoBehaviour {
    public string _types = "old_gang|military";
    public GameObject parentObj;
    public float Xmin = -1.5f;
    public float Xmax = 1.5f;

    private List<GameObject> spawnList = new List<GameObject>();

    // Start is called before the first frame update
    void Start() {
        string[] types;
        if (this._types.Contains("|")) {
            types = this._types.Split('|');
        } else {
            types = new string[1];
            types[0] = this._types;
        }
        int difficult = 1;

        if (GameObject.FindGameObjectWithTag("Global").GetComponent<LocationObjectiveDestroy>() != null) {
            difficult = GameObject.FindGameObjectWithTag("Global").GetComponent<LocationObjectiveDestroy>().getRandomLocationOptions().difficult;
        }

        int chance = Random.Range(0, 100);
        int chanceDiff = 100 - (difficult * 10);
        if (chance > chanceDiff) {
            StartCoroutine(prepareSection(
                types[Random.Range(0, types.Length)],
                difficult
            ));
        } else  {
            Destroy(gameObject);
        }
        
    }

    private IEnumerator prepareSection(string type, int difficult) {
        int maxSpawnObj = difficult + 1;
        if (maxSpawnObj > 4) maxSpawnObj = 4;
        maxSpawnObj = Random.Range(1, maxSpawnObj);


        string sectionId = "charecters/human/human";
        L.S("AISectionDoorController:prepareSection");
        ResourceRequest resourcesRequest = Resources.LoadAsync(sectionId, typeof(GameObject));
        while (!resourcesRequest.isDone)
            yield return null;

        GameObject AIsectionObj = resourcesRequest.asset as GameObject;
        for (int i = 0; i < maxSpawnObj; i++) {
            GameObject AIitem = Instantiate(
                AIsectionObj,
                new Vector3(-1000, -1000, 0),
                this.gameObject.transform.rotation,
                this.gameObject.transform
            );
            spawnList.Add(AIitem);
        }
        bool isHeavy = false;
        if (Random.Range(0, 100) > 90) isHeavy = true;
        GetComponent<AISectionGenerator>().generate(type, isHeavy, false);

        foreach (GameObject obj in spawnList) obj.SetActive(false);
    }

    void OnTriggerEnter2D(Collider2D col) {
        if (col.gameObject.transform.parent.GetComponent<CharecterController>() == null) return;
        if (col.gameObject.transform.parent.GetComponent<CharecterController>().isPlayer) {
            Destroy(GetComponent<BoxCollider2D>());
            StartCoroutine(spawnSection(col.gameObject.transform.parent.gameObject));
        }
    }

    private IEnumerator spawnSection(GameObject player) {
        if (parentObj.GetComponent<DoorAction>() != null)
            parentObj.GetComponent<DoorAction>().activate();
        
        foreach (GameObject obj in spawnList) {
            yield return new WaitForSeconds(1.5f);
            obj.SetActive(true);
            obj.transform.position = parentObj.transform.position;
            obj.GetComponent<CharecterController>().getAI().setTarget(
                player
            );
        }

        if (parentObj.GetComponent<DoorAction>() != null)
            parentObj.GetComponent<DoorAction>().activate();

        Destroy(this);
    }
}