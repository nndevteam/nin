﻿using UnityEngine;

public class AISectionGenerator : MonoBehaviour {
    private bool _isGenerated = false;
    public void generate(string type, bool isHeavy, bool isRequired) {
        RandomLocationOptions randomLocationOptions = null;
        if (GameObject.FindGameObjectWithTag("Global").GetComponent<LocationObjectiveDestroy>() != null) {
            randomLocationOptions = GameObject.FindGameObjectWithTag("Global").GetComponent<LocationObjectiveDestroy>().getRandomLocationOptions();
        }

        if (randomLocationOptions == null) return;

        string cosmeticsType = CosmeticsRandomSystem.TYPE_GANG;
        if (type.Equals("military")) cosmeticsType = CosmeticsRandomSystem.TYPE_MILITARY;
        

        foreach (Transform enemy in transform) {
            if (enemy.gameObject.GetComponent<CharecterController>() == null) continue;
            int difficult = randomLocationOptions.difficult;

            if (isHeavy) difficult = randomLocationOptions.difficult + 2;
            if (Random.Range(0, 100) > 70) difficult = randomLocationOptions.difficult + Random.Range(0, 2);

            enemy.gameObject.GetComponent<CharecterSkin>().generateByType(cosmeticsType, difficult);

            GameObject.FindGameObjectWithTag("WeaponData").GetComponent<WeaponsRandomSystem>().prepareByDifficult(difficult);
            GameObject weaponEnemy = Instantiate(GameObject.FindGameObjectWithTag("WeaponData").GetComponent<WeaponsRandomSystem>().getByTypeOwner(type, isHeavy));
            weaponEnemy.GetComponent<FireArms>().ammoCount = int.MaxValue;
            enemy.gameObject.GetComponent<CharecterController>().replaceWeapon(weaponEnemy);
            enemy.gameObject.GetComponent<CharecterController>().selectWeapon(weaponEnemy.GetComponent<FireArms>().hands);
            
            if (isRequired && GameObject.FindGameObjectWithTag("Global").GetComponent<LocationObjectiveDestroy>() != null) {
                GameObject.FindGameObjectWithTag("Global").GetComponent<LocationObjectiveDestroy>().addTarget(enemy.gameObject);
            }
        }
        if (isRequired) {
            GameObject.FindGameObjectWithTag("Global").GetComponent<SceneLoaderInGame>().onLoadEnd();
            L.S("LocationRandomSystem:onLoadEnd");
        }
        _isGenerated = true;
    }

    public bool isGenerated() {
        return _isGenerated;
    }
}
