﻿using System.Collections;
using UnityEngine;

public class AISectionController : MonoBehaviour {
    public string _types = "old_gang|military";
    public int _variantX = 123;
    public bool isHeavy = false;
    public bool isRequired = false;

    void Start() {
        string[] types;
        if (this._types.Contains("|")) {
            types = this._types.Split('|');
        } else {
            types = new string[1];
            types[0] = this._types;
        }

        int difficult = 1;
        if (GameObject.FindGameObjectWithTag("Global").GetComponent<LocationObjectiveDestroy>() != null) {
            difficult = GameObject.FindGameObjectWithTag("Global").GetComponent<LocationObjectiveDestroy>().getRandomLocationOptions().difficult;
        }
        StartCoroutine(prepareSection(
            types[Random.Range(0, types.Length)],
            difficult
        ));
    }

    private IEnumerator prepareSection(string type, int difficult) {
        int maxSpawnObj = 2;
        if (_variantX == 2) maxSpawnObj = 3;
        if (_variantX == 3) maxSpawnObj = 4;
        if (! isRequired) {
            if (_variantX == 2) maxSpawnObj = Random.Range(1, maxSpawnObj + 1);
            if (_variantX == 3) maxSpawnObj = Random.Range(1, maxSpawnObj + 1);
        }
        if (difficult < 7) {
            maxSpawnObj = Random.Range(1, maxSpawnObj + 1);
        } else {
            maxSpawnObj = Random.Range(2, maxSpawnObj + 1);
        }

        string sectionId = "charecters/human/human";
        L.S("AISectionController:prepareSection");
        ResourceRequest resourcesRequest = Resources.LoadAsync(sectionId, typeof(GameObject));
        while (!resourcesRequest.isDone)
            yield return null;

        GameObject AIsectionObj = resourcesRequest.asset as GameObject;
        float posXMax = this.gameObject.transform.position.x + (_variantX * 3);
        float posXMin = this.gameObject.transform.position.x - (_variantX * 3);
        float posYMax = this.gameObject.transform.position.y + 1.5f;
        float posYMin = this.gameObject.transform.position.y - 1.5f;

        for (int i = 0; i < maxSpawnObj; i++) {
            GameObject AIitem = Instantiate(
                AIsectionObj,
                new Vector3(Random.Range(posXMin, posXMax), Random.Range(posYMin, posYMax), 0),
                this.gameObject.transform.rotation,
                this.gameObject.transform
            );
            if (Random.Range(0, 100) > 40) 
                AIitem.GetComponent<CharecterController>().setSide(180);
            
        }
        GetComponent<AISectionGenerator>().generate(type, isHeavy, isRequired);
        Destroy(this);
    }
}
