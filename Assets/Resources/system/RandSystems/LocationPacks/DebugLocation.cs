﻿
public class DebugLocation {
    public string locationPrefix = "test_location";
    public int direction = 1; //-1 for down
    public int baseBlockCount = 1;
    public int centerBlockCount = 1;
    public int endBlockCount = 1;
}
