﻿using System.Linq;
using UnityEngine;

public class WeaponsRandomSystem : MonoBehaviour {
    public GameObject[] pistols;
    public GameObject[] smg;
    public GameObject[] rifles;

    private GameObject[] listPistols;
    private GameObject[] listSMG;
    private GameObject[] listRifles;


    public void prepareByDifficult(int difficult) {
        if (difficult <= 5) {
            listPistols = pistols.Take(4).ToArray();
            listSMG = smg.Take(3).ToArray();
            listRifles = rifles.Take(1).ToArray();
        }
        if (difficult > 5 && difficult <= 10) {
            listPistols = pistols;
            listSMG = smg.Take(5).ToArray();
            listRifles = rifles.Take(2).ToArray();
        }
        if (difficult > 10) {
            listPistols = pistols;
            listSMG = smg;
            listRifles = rifles;
        }
    }

    public GameObject getPistol() {
        return listPistols[Random.Range(0, listPistols.Length)];
    }

    public GameObject getSMG() {
        return listSMG[Random.Range(0, listSMG.Length)];
    }

    public GameObject getRifle() {
        return listRifles[Random.Range(0, listRifles.Length)];
    }

    public GameObject getFirstWeapon() {
        return listPistols[Random.Range(0, listPistols.Length)];
    }

    public GameObject getSecondWeapon() {
        if (Random.Range(0, 100) < 70) {
            return getSMG();
        } else {
            return getRifle();
        }
    }

    public GameObject getByTypeOwner(string type, bool isHeavy) {
        //old_gang|military
        int smgChance = 100;

        if (!isHeavy && type.Equals("old_gang")) {
            if (Random.Range(0, 100) < 30)  {
                return getSMG();
            } else {
                return getPistol();
            }
        } else if (!isHeavy && type.Equals("military")) {
            smgChance = 60;
            if (Random.Range(0, 100) < smgChance) {
                return getSMG();
            } else {
                return getRifle();
            }
        } else {
            //isHeavy
            smgChance = 20;
            if (Random.Range(0, 100) < smgChance) {
                return getSMG();
            } else {
                return getRifle();
            }
        }
    }
}
