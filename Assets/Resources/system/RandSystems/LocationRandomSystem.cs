﻿using System.Collections;
using UnityEngine;

public class LocationRandomSystem : MonoBehaviour {
    private GameObject world;

    public void generate(RandomLocationOptions options) {
        world = GameObject.FindGameObjectWithTag("World");
        L.S("LocationRandomSystem:generate");
        L.S("LocationRandomSystem Difficult : " + options.difficult);
        L.S("LocationRandomSystem Type : " + options.locationPrefix);
        L.S("LocationRandomSystem Floor BLock Count : " + options.blockCount);

        StartCoroutine(loadBaseLocation(options));
    }

    private IEnumerator loadBaseLocation(RandomLocationOptions options) {
        string locationId = "locations/packs/" + options.locationPrefix + "_base_" + Random.Range(1, options.baseBlockCount + 1);
        L.S("LocationRandomSystem:loadBaseLocation: " + locationId);
        ResourceRequest resourcesRequest = Resources.LoadAsync(locationId, typeof(GameObject));

        while (!resourcesRequest.isDone)
            yield return null;

        GameObject locationObj = resourcesRequest.asset as GameObject;
        GameObject baseLocation = Instantiate(
             locationObj,
             new Vector3(0,0,0),
             world.transform.rotation,
             world.transform
        );

        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PerformanceOptimization>().addPart(baseLocation);

        if (options.blockCount == 0){
            StartCoroutine(loadEndLocation(
                1,
                options,
                baseLocation.GetComponent<LocationData>()
            ));
        } else {
            StartCoroutine(loadCenterLocation(
                1,
                options,
                baseLocation.GetComponent<LocationData>()
            ));
        }
    }

    private IEnumerator loadCenterLocation(int currentCount, RandomLocationOptions options, LocationData parentLinkedLocation) {
        yield return new WaitForSeconds(0.1f);

        string locationId = "locations/packs/" + options.locationPrefix + "_center_" + Random.Range(1, options.centerBlockCount + 1);
        L.S("LocationRandomSystem:loadLocation: " + locationId);
        ResourceRequest resourcesRequest = Resources.LoadAsync(locationId, typeof(GameObject));
        while (!resourcesRequest.isDone)
            yield return null;

        GameObject locationObj = resourcesRequest.asset as GameObject;
        GameObject location = Instantiate(
             locationObj,
             new Vector3(0, currentCount * 39.6f * options.direction, 0),
             world.transform.rotation,
             world.transform
        );

        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PerformanceOptimization>().addPart(location);

        LocationData linkedLocation = location.GetComponent<LocationData>();
        if (parentLinkedLocation.connectionOutAction != null && linkedLocation.connectionInPosition != null) {
            parentLinkedLocation.connectionOutAction.GetComponent<ActionController>().iteractAction = linkedLocation.connectionInPosition;
        }
        if (linkedLocation.connectionInAction != null && parentLinkedLocation.connectionOutPosition != null) {
            linkedLocation.connectionInAction.GetComponent<ActionController>().iteractAction = parentLinkedLocation.connectionOutPosition;
        }

        if (currentCount == options.blockCount) {
            StartCoroutine(loadEndLocation(
                currentCount + 1,
                options,
                linkedLocation
            ));
        } else {
            StartCoroutine(loadCenterLocation(
                currentCount + 1,
                options,
                linkedLocation
            ));
        }
    }

    private IEnumerator loadEndLocation(int currentCount, RandomLocationOptions options, LocationData parentLinkedLocation) {
        yield return new WaitForSeconds(0.1f);

        string locationId = "locations/packs/" + options.locationPrefix + "_end_" + Random.Range(1, options.endBlockCount + 1);
        L.S("LocationRandomSystem:loadLocation: " + locationId);
        ResourceRequest resourcesRequest = Resources.LoadAsync(locationId, typeof(GameObject));
        while (!resourcesRequest.isDone)
            yield return null;

        GameObject locationObj = resourcesRequest.asset as GameObject;
        GameObject location = Instantiate(
             locationObj,
             new Vector3(0, currentCount * 39.6f * options.direction, 0),
             world.transform.rotation,
             world.transform
        );

        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PerformanceOptimization>().addPart(location);

        LocationData linkedLocation = location.GetComponent<LocationData>();
        if (parentLinkedLocation.connectionOutAction != null && linkedLocation.connectionInPosition != null) {
            parentLinkedLocation.connectionOutAction.GetComponent<ActionController>().iteractAction = linkedLocation.connectionInPosition;
        }
        if (linkedLocation.connectionInAction != null && parentLinkedLocation.connectionOutPosition != null) {
            linkedLocation.connectionInAction.GetComponent<ActionController>().iteractAction = parentLinkedLocation.connectionOutPosition;
        }

        StartCoroutine(loadPlayerObjects(options));
    }

    private IEnumerator loadPlayerObjects(RandomLocationOptions options) {
        ResourceRequest resourcesRequest = Resources.LoadAsync("charecters/human/human", typeof(GameObject));

        while (!resourcesRequest.isDone)
            yield return null;

        GameObject charecterObj = resourcesRequest.asset as GameObject;

        GameObject charecter = Instantiate(
              charecterObj,
              new Vector3(0, 5.2f, 0),
              world.transform.rotation,
              world.transform
        );
        int difficult = options.difficult + 5;
        charecter.GetComponent<CharecterSkin>().generateByType(CosmeticsRandomSystem.TYPE_MILITARY, difficult);

        GameObject.FindGameObjectWithTag("WeaponData").GetComponent<WeaponsRandomSystem>().prepareByDifficult(difficult);
        GameObject weapon1 = Instantiate(GameObject.FindGameObjectWithTag("WeaponData").GetComponent<WeaponsRandomSystem>().getFirstWeapon());
        GameObject weapon2 = Instantiate(GameObject.FindGameObjectWithTag("WeaponData").GetComponent<WeaponsRandomSystem>().getSecondWeapon());
        charecter.GetComponent<CharecterController>().replaceWeapon(weapon1);
        charecter.GetComponent<CharecterController>().replaceWeapon(weapon2);
        charecter.GetComponent<CharecterController>().selectWeapon(2);
        L.S("WeaponData 1 : " + weapon1.name);
        L.S("WeaponData 2 : " + weapon2.name);
        
        charecter.GetComponent<CharecterController>().setIsPlayer();
        //charecter.GetComponent<CharecterStats>().health = float.MaxValue;
        //weapon2.GetComponent<FireArms>().ammoCount = int.MaxValue;
    }
}