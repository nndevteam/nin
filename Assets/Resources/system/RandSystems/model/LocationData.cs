﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationData : MonoBehaviour{
    public GameObject connectionInAction;
    public GameObject connectionInPosition;

    public GameObject connectionOutAction;
    public GameObject connectionOutPosition;
}
