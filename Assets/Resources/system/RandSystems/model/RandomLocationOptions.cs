﻿
public class RandomLocationOptions {
    public int difficult = 1;
    public int blockCount;

    public string locationPrefix;
    public int direction;
    public int baseBlockCount;
    public int centerBlockCount;
    public int endBlockCount;

    public RandomLocationOptions(OldGangLocation location)  {
        this.locationPrefix = location.locationPrefix;
        this.direction = location.direction;
        this.baseBlockCount = location.baseBlockCount;
        this.centerBlockCount = location.centerBlockCount;
        this.endBlockCount = location.endBlockCount;
    }

    public RandomLocationOptions(DebugLocation location) {
        this.locationPrefix = location.locationPrefix;
        this.direction = location.direction;
        this.baseBlockCount = location.baseBlockCount;
        this.centerBlockCount = location.centerBlockCount;
        this.endBlockCount = location.endBlockCount;
    }
}
