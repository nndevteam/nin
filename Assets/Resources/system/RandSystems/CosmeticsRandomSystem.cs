﻿using UnityEngine;

public class CosmeticsRandomSystem : MonoBehaviour {
    public static string TYPE_MILITARY = "TYPE_MILITARY";
    public static string TYPE_GANG = "TYPE_GANG";

    public static string TYPE_ARMOR_LIGHT = "TYPE_ARMOR_LIGHT";
    public static string TYPE_ARMOR_HEAVY = "TYPE_ARMOR_HEAVY";

    public Sprite[] listHeads;
    public Sprite[] listHeadHair;
    public Sprite[] listHeadBeard;

    public Sprite[] listHeadMilitary;
    public GameObject[] listTorsoMilitary;
    public GameObject[] listBottomMilitary;

    public Sprite[] listHeadGang;
    public GameObject[] listTorsoGang;
    public GameObject[] listBottomGang;

    public GameObject[] listArmorTorsoLight;
    public GameObject[] listArmorTorsoHeavy;
    public GameObject[] listArmorBottomLight;
    public GameObject[] listArmorHeadLight;
    public GameObject[] listArmorHeadHeavy;

    public Sprite[] getRHead(string type) {
        Sprite[] result = new Sprite[3];
        //0 - base
        //1 - hair
        //2 - beard
        result[0] = listHeads[Random.Range(0, listHeads.Length)];
        
        //Chance of hair
        int hairChance = 90;
        if (type.Equals(TYPE_MILITARY)) 
            hairChance = 60;
        int chance = Random.Range(0, 101);
        if (chance < hairChance) {
            result[1] = listHeadHair[Random.Range(0, listHeadHair.Length)];
        } else {
            result[1] = null;
        }

        //Chance of beard
        int beardChance = 50;
        if (type.Equals(TYPE_MILITARY))
            beardChance = 70;
        chance = Random.Range(0, 101);
        if (chance < beardChance) {
            result[2] = listHeadBeard[Random.Range(0, listHeadBeard.Length)];
        } else {
            result[2] = null;
        }

        return result;
    }

    public Sprite getRHeadWear(string type) {
        int hasChance = 75;
        if (type.Equals(TYPE_MILITARY))
            hasChance = 50;

        int chance = Random.Range(0, 101);
        if (chance < hasChance) {
            if (type.Equals(TYPE_MILITARY))
                return listHeadMilitary[Random.Range(0, listHeadMilitary.Length)];

            if (type.Equals(TYPE_GANG))
                return listHeadGang[Random.Range(0, listHeadGang.Length)];
        }

        return null;
    }

    public GameObject getRTorso(string type) {
        if (type.Equals(TYPE_MILITARY))
            return listTorsoMilitary[Random.Range(0, listTorsoMilitary.Length)];

        if (type.Equals(TYPE_GANG))
            return listTorsoGang[Random.Range(0, listTorsoGang.Length)];

        return null;
    }

    public GameObject getRBottom(string type) {
        if (type.Equals(TYPE_MILITARY))
            return listBottomMilitary[Random.Range(0, listBottomMilitary.Length)];

        if (type.Equals(TYPE_GANG))
            return listBottomGang[Random.Range(0, listBottomGang.Length)];

        return null;
    }

    public GameObject getArmorTorso(string type) {
        GameObject[] list = listArmorTorsoLight;
        if (type.Equals(TYPE_ARMOR_HEAVY)) list = listArmorTorsoHeavy;
        if (list == null) return null;
        return list[Random.Range(0, list.Length)];
    }

    public GameObject getArmorBottom(string type)  {
        GameObject[] list = listArmorBottomLight;
        if (type.Equals(TYPE_ARMOR_HEAVY)) list = listArmorBottomLight;
        if (list == null) return null;
        return list[Random.Range(0, list.Length)];
    }

    public GameObject getArmorHead(string type) {
        GameObject[] list = listArmorHeadLight;
        if (type.Equals(TYPE_ARMOR_HEAVY)) list = listArmorHeadHeavy;
        if (list == null) return null;
        return list[Random.Range(0, list.Length)];
    }
}
