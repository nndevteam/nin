﻿using UnityEngine;

public class AnimationEventsCatch : MonoBehaviour {
    public void CatchAnimationEvents(string message) {
        if (message.Equals("destroy")) Destroy(gameObject);
    }
}
