﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class PerformanceOptimization : MonoBehaviour {
    public bool isActive = false;
    private List<GameObject> location = new List<GameObject>();

    void Start() {
        isActive = false;
    }

    public void addPart(GameObject obj) {
        location.Add(obj);
    }

    public void setActive() {
        isActive = true;
    }

    void LateUpdate() {
        if (! isActive) return;

        foreach (GameObject obj in location) {
            float posObj = Mathf.Abs(obj.transform.position.y);
            float posCamera = Mathf.Abs(transform.position.y);
            if (posCamera + 38f < posObj || posCamera - 65f > posObj) {
                obj.transform.GetChild(0).gameObject.SetActive(false);
            } else {
                obj.transform.GetChild(0).gameObject.SetActive(true);
            }
        }
    }
}
