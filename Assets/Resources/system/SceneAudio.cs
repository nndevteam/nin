﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneAudio : MonoBehaviour {
    public GameObject idleAudio;
    public GameObject actionAudio;

    public List<AudioClip> idleClips;
    public List<AudioClip> actionClips;

    private bool isActionState = false;

    void Start() {
        if (!SavedSettings.getInstance().isMusic()) {
            idleAudio.GetComponent<AudioSource>().Stop();
            idleAudio.GetComponent<AudioSource>().mute = true;
            actionAudio.GetComponent<AudioSource>().Stop();
            actionAudio.GetComponent<AudioSource>().mute = true;
        } else {
            updateIdleClips();
            updateActionClips();
        }
    }

    public void toAction() {
        if (! isActionState) {
            updateActionClips();
        }
        isActionState = true;
    }

    public void toIdle() {
        if (isActionState) {
            updateIdleClips();
        }
        isActionState = false;
    }

    void FixedUpdate() {
        if (!SavedSettings.getInstance().isMusic()) return;

        if (isActionState) {
            if (actionAudio.GetComponent<AudioSource>().volume < 0.65f)
                actionAudio.GetComponent<AudioSource>().volume += 0.005f;

            if (idleAudio.GetComponent<AudioSource>().volume > 0.01f)
                idleAudio.GetComponent<AudioSource>().volume -= 0.005f;
        } else {
            if (idleAudio.GetComponent<AudioSource>().volume < 0.51f)
                idleAudio.GetComponent<AudioSource>().volume += 0.005f;

            if (actionAudio.GetComponent<AudioSource>().volume > 0.01f)
                actionAudio.GetComponent<AudioSource>().volume -= 0.005f;
        }

        GameObject world = GameObject.FindGameObjectWithTag("World");
        bool isIdle = true;
        foreach (AI sceneObj in world.GetComponentsInChildren<AI>()) {
            if (sceneObj.target != null) isIdle = false;
        }
        if (isIdle) {
            toIdle();
        } else {
            toAction();
        }
    }

    private void updateIdleClips() {
        if (!SavedSettings.getInstance().isMusic()) return;

        if (idleClips != null && idleClips.Count > 0) {
            idleAudio.GetComponent<AudioSource>().clip = idleClips[Random.Range(0, idleClips.Count)];
            idleAudio.GetComponent<AudioSource>().Play();
        } else {
            idleAudio.GetComponent<AudioSource>().Stop();
            idleAudio.GetComponent<AudioSource>().mute = true;
        }
    }

    private void updateActionClips() {
        if (!SavedSettings.getInstance().isMusic()) return;

        if (actionClips != null && actionClips.Count > 0) {
            actionAudio.GetComponent<AudioSource>().clip = actionClips[Random.Range(0, idleClips.Count)];
            actionAudio.GetComponent<AudioSource>().Play();
        } else {
            actionAudio.GetComponent<AudioSource>().Stop();
            actionAudio.GetComponent<AudioSource>().mute = true;
        }
    }
}
