﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoader : MonoBehaviour {
    public GameObject fade;
    public bool fadeOutOnStart = false;


    void Start() {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        fade.SetActive(true);
        if (fadeOutOnStart) {
            StartCoroutine(fadeInOut(false, null));
        }
    }

    public void launchById(string oid) {
        StartCoroutine(fadeInOut(true, oid));
    }

    IEnumerator _launchById(string _scene) {
        AsyncOperation loader = SceneManager.LoadSceneAsync(_scene);
        while (!loader.isDone) {
            yield return null;
        }
    }

    IEnumerator fadeInOut(bool isIn, string _scene) {
        float duration = 1f;
        float counter = 0f;

        float a, b;
        if (isIn) {
            a = 0;
            b = 1;
        } else {
            a = 1;
            b = 0;
        }

        Image fader = fade.GetComponent<Image>();

        while (counter < duration) {
            counter += Time.deltaTime;
            float alpha = Mathf.Lerp(a, b, counter / duration);

            fader.color = new Color(fader.color.r, fader.color.g, fader.color.b, alpha);

            yield return null;
        }

        if (_scene != null) {
            StartCoroutine(_launchById(_scene));
        }
    }
}
