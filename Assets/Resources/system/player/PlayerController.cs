﻿using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    private GameObject playerObj;
    private CameraController cameraController;
    // Start is called before the first frame update
    void Start() {
        cameraController = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraController>();
    }

    // Update is called once per frame
    void Update() {
        if (Constants.isMobile) return;
        if (playerObj == null || playerObj.GetComponent<CharecterController>() == null) return;

        playerObj.GetComponent<CharecterController>().moveBy(Input.GetAxis("LeftRight"));

        playerObj.GetComponent<CharecterController>().setTarget(Input.GetAxis("UpDown"));

        if (Input.GetAxis("Shoot") != 0 && playerObj.GetComponent<CharecterController>().isAction("isCoverStand")) {
            playerObj.GetComponent<CharecterController>().handleCover(0);
        }
        
        playerObj.GetComponent<CharecterController>().iteractWeapon(Input.GetAxis("Shoot"));
        playerObj.GetComponent<CharecterController>().doAction("aim", Input.GetAxis("Aim") != 0 || Input.GetAxis("Shoot") != 0);
        cameraController.setAimZoom(Input.GetAxis("Aim") + Input.GetAxis("Shoot"));

        if (Input.GetButtonDown("Cover")) 
            playerObj.GetComponent<CharecterController>().handleCover(Input.GetAxis("UpDown"));


        if (Input.GetButtonDown("Action")) {
            playerObj.GetComponent<CharecterController>().startAction();
        }

        if (Input.GetButtonDown("SelectWeapon1")) {
            playerObj.GetComponent<CharecterController>().selectWeapon(1);
        }

        if (Input.GetButtonDown("SelectWeapon2")) {
            playerObj.GetComponent<CharecterController>().selectWeapon(2);
        }
    }

    public void setPlayerObj(GameObject obj) {
        playerObj = obj;
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraController>().setFollowObj(playerObj);
    }

    public GameObject getPlayerObj() {
        return playerObj;
    }
}
