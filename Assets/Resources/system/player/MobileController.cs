﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MobileController : MonoBehaviour {
    public FloatingJoystick joystick;
    public GameObject actionView;
    public Text actionViewText;
    public GameObject topCover;
    public GameObject bottomCover;

    private GameObject playerObj;
    private bool isShooting = false;
    private CameraController cameraController;

    void Start() {
        cameraController = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraController>();

        actionView.SetActive(false);
        topCover.SetActive(false);
        bottomCover.SetActive(false);
    }

    public void onShowHideCoverTopUI(bool isShow) {
        topCover.SetActive(isShow);
    }
    public void onShowHideCoverBottomUI(bool isShow) {
        bottomCover.SetActive(isShow);
    }
    public void onShowHideActionUI(bool isShow, string text) {
        actionViewText.text = text;
        actionView.SetActive(isShow);
    }


    public void onCoverTop() {
        playerObj = GameObject.FindGameObjectWithTag("Global").GetComponent<PlayerController>().getPlayerObj();
        if (playerObj == null || playerObj.GetComponent<CharecterController>() == null) return;

        playerObj.GetComponent<CharecterController>().handleCover(1);
    }

    public void onCoverBottom() {
        playerObj = GameObject.FindGameObjectWithTag("Global").GetComponent<PlayerController>().getPlayerObj();
        if (playerObj == null || playerObj.GetComponent<CharecterController>() == null) return;

        playerObj.GetComponent<CharecterController>().handleCover(-1);
    }
    public void onAction() {
        playerObj = GameObject.FindGameObjectWithTag("Global").GetComponent<PlayerController>().getPlayerObj();
        if (playerObj == null || playerObj.GetComponent<CharecterController>() == null) return;

        playerObj.GetComponent<CharecterController>().startAction();
    }

    public void onShoot(bool _isShooting) {
        this.isShooting = _isShooting;
    }


    void Update() {
        playerObj = GameObject.FindGameObjectWithTag("Global").GetComponent<PlayerController>().getPlayerObj();
        if (playerObj == null || playerObj.GetComponent<CharecterController>() == null) return;

        playerObj.GetComponent<CharecterController>().moveBy(joystick.Horizontal);
        if (joystick.Horizontal != 0) 
            playerObj.GetComponent<CharecterController>().handleCover(0);

        playerObj.GetComponent<CharecterController>().setTarget(0);

        if (Input.GetButtonDown("SelectWeapon1")) {
            playerObj.GetComponent<CharecterController>().selectWeapon(1);
        }

        if (Input.GetButtonDown("SelectWeapon2")) {
            playerObj.GetComponent<CharecterController>().selectWeapon(2);
        }
        if (isShooting) {
            cameraController.setAimZoom(1);
            if (playerObj.GetComponent<CharecterController>().getActiveWeapon().GetComponent<FireArms>().ammoCount == 0 && playerObj.GetComponent<CharecterController>().getActiveWeapon().GetComponent<FireArms>().ammoClip == 0) {
                playerObj.GetComponent<CharecterController>().selectWeapon(1);
                return;
            }
            if (playerObj.GetComponent<CharecterController>().isAction("isCoverStand"))
                playerObj.GetComponent<CharecterController>().handleCover(0);

            playerObj.GetComponent<CharecterController>().doAction("aim", true);
            playerObj.GetComponent<CharecterController>().iteractWeapon(1);
        } else {
            cameraController.setAimZoom(0);
            playerObj.GetComponent<CharecterController>().doAction("aim", false);
            playerObj.GetComponent<CharecterController>().iteractWeapon(0);
        }
    }
}
