﻿using System.Collections;
using UnityEngine;

public class L {
    public static void S(string message) {
        if (! Constants.isDebug) return;
        Debug.Log(message);
    }
}
