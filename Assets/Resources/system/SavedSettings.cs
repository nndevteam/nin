﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

[Serializable]
public class SavedSettings {
    public static SavedSettings instance = null;
    private static string saveName = Application.persistentDataPath + "/settings.nnd";

    private bool _isBloody = ! Constants.isMobile;
    private bool _isSound = true;
    private bool _isMusic = true;

    public static SavedSettings getInstance() {
        if (instance == null) {
            if (File.Exists(saveName)) {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                FileStream file = File.Open(saveName, FileMode.Open);
                instance = (SavedSettings)binaryFormatter.Deserialize(file);
                file.Close();
            } else {
                instance = new SavedSettings();
            }
        }
        return instance;
    }


    private void save() {
        L.S("Sound :: " + (_isSound ? "true" : "false"));
        L.S("Music :: " + (_isMusic ? "true" : "false"));
        L.S("Bloody :: " + (_isBloody ? "true" : "false"));
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        FileStream file = File.Create(saveName);
        binaryFormatter.Serialize(file, instance);
        file.Close();
    }


    public bool isSound() {
        return _isSound;
    }
    public void setSound(bool val) {
        _isSound = val;
        save();
    }
    public bool isMusic() {
        return _isMusic;
    }

    public void setMusic(bool val) {
        _isMusic = val;
        save();
    }

    public bool isBloody() {
        return _isBloody;
    }

    public void setBloody(bool val) {
        _isBloody = val;
        save();
    }

}
