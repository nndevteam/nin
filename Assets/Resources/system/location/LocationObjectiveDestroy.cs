﻿using System.Collections.Generic;
using UnityEngine;

public class LocationObjectiveDestroy : MonoBehaviour {
    private bool _isCompleted = false;
    private bool _isFailed = false;
    private RandomLocationOptions randomLocationOptions;

    public List<GameObject> targets = new List<GameObject>();

    public void addTarget(GameObject target) {
        if (_isCompleted || _isFailed) return;
        targets.Add(target);
    }

    public void removeTarget(GameObject target) {
        if (_isCompleted || _isFailed) return;
        targets.Remove(target);
        if (targets.Count == 0) {
            _isCompleted = true;
            //END SCREEN
            StateData.getInstance().setFloorDifficult(randomLocationOptions.difficult);
            L.S("END SCREEN :: isCompleted");
            if (GetComponent<MissionMenu>() != null) StartCoroutine(GetComponent<MissionMenu>().onWin());
        }
    }

    public void onFailed() {
        if (_isCompleted || _isFailed) return;
        _isFailed = true;
        //END SCREEN
        L.S("END SCREEN :: isFailed");
        if (GetComponent<MissionMenu>() != null) StartCoroutine(GetComponent<MissionMenu>().onFail());
    }

    public bool isCompleted() {
        return _isCompleted;
    }

    public bool isFailed() {
        return _isFailed;
    }
    public void setRandomLocationOptions(RandomLocationOptions opt) {
        randomLocationOptions = opt;
    }

    public RandomLocationOptions getRandomLocationOptions() {
        return randomLocationOptions;
    }
}
