﻿using UnityEngine;

public class TrainingObjective : MonoBehaviour {
    public int id;
    public GameObject task;

    void OnCollisionEnter2D(Collision2D coll) {
        if (coll.gameObject.GetComponent<Bullet>() != null) {
            if (id == 1) task.GetComponent<TrainingTask>().target1Count++;
            if (id == 2) task.GetComponent<TrainingTask>().target2Count++;
            if (id == 3) task.GetComponent<TrainingTask>().target3Count++;
        }
    }
}
