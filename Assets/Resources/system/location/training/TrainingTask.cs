﻿using System.Collections;
using UnityEngine;

public class TrainingTask : MonoBehaviour {
    public GameObject endDialog;
    public GameObject endDialogFailed;
    public int target1Count = 0;
    public int target2Count = 0;
    public int target3Count = 0;
    private bool isEnded = false;

    void Start() {
        
    }

    
    void LateUpdate() {
        if (isEnded) return;
        int counter = 0;
        if (target1Count > 2) counter++;
        if (target2Count > 2) counter++;
        if (target3Count > 2) counter++;
        if (counter > 1) {
            isEnded = true;
            endDialog.GetComponent<DialogAction>().activate(null);
            StartCoroutine(onEnd());
        }
        if (GameObject.FindGameObjectWithTag("Global").GetComponent<PlayerController>().getPlayerObj() != null) {
            GameObject activeWeapon = GameObject.FindGameObjectWithTag("Global").GetComponent<PlayerController>().getPlayerObj().GetComponent<CharecterController>().getActiveWeapon();
            if (activeWeapon != null && activeWeapon.GetComponent<FireArms>().ammoCount == 0 && activeWeapon.GetComponent<FireArms>().ammoClip == 0) {
                isEnded = true;
                endDialogFailed.GetComponent<DialogAction>().activate(null);
                StartCoroutine(onEndFailed());
            }
        }
    }

    private IEnumerator onEnd() {
        StateData.getInstance().setComplete("training");
        yield return new WaitForSeconds(8);

        GameObject.FindGameObjectWithTag("Global").GetComponent<SceneLoader>().launchById("menu_hub");
    }

    private IEnumerator onEndFailed() {
        StateData.reset();
        yield return new WaitForSeconds(8);

        GameObject.FindGameObjectWithTag("Global").GetComponent<SceneLoader>().launchById("menu");
    }
}
