﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SceneLoaderInGame : MonoBehaviour {
    public GameObject progressView;
    public GameObject fade;

    void Start() {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        fade.SetActive(true);
        progressView.SetActive(true);

        RandomLocationOptions randomLocationOptions = SessionData.getInstance().getRandomLocationOptions();
        /*//====================
        int floorDiff = 1;
        randomLocationOptions = new RandomLocationOptions(
                new OldGangLocation()
        );
        randomLocationOptions.blockCount = Random.Range((int)(floorDiff / 1.5f), floorDiff + 2);
        randomLocationOptions.difficult = floorDiff;
        //====================
        */
        if (randomLocationOptions == null) {
            GetComponent<MissionMenu>().onExit();
            return;
        }

        if (GameObject.FindGameObjectWithTag("Global").GetComponent<LocationObjectiveDestroy>() != null) {
            GameObject.FindGameObjectWithTag("Global").GetComponent<LocationObjectiveDestroy>().setRandomLocationOptions(randomLocationOptions);
        }
        GameObject.FindGameObjectWithTag("LocationData").GetComponent<LocationRandomSystem>().generate(randomLocationOptions);
    }

    public void onLoadEnd() {
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PerformanceOptimization>().setActive();
        progressView.SetActive(false);
        StartCoroutine(fadeInOut(false));
        Time.timeScale = 1;
        if (StateData.getInstance().isShowControlHint()) {
            GetComponent<MissionMenu>().onControlHint();
        }
    }

    IEnumerator fadeInOut(bool isIn) {
        float duration = 1f;
        float counter = 0f;

        float a, b;
        if (isIn) {
            a = 0;
            b = 1;
        } else {
            a = 1;
            b = 0;
        }

        Image fader = fade.GetComponent<Image>();

        while (counter < duration) {
            counter += Time.deltaTime;
            float alpha = Mathf.Lerp(a, b, counter / duration);

            fader.color = new Color(fader.color.r, fader.color.g, fader.color.b, alpha);

            yield return null;
        }
    }
}
