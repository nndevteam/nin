﻿using UnityEngine;

public class AudioRandom : MonoBehaviour {
    public bool isAutoStart = true;
    public AudioClip[] sfx;

    void Start() {
        if (! SavedSettings.getInstance().isMusic()) {
            GetComponent<AudioSource>().Stop();
            GetComponent<AudioSource>().mute = true;
        } else if (sfx != null && sfx.Length > 0) {
            GetComponent<AudioSource>().clip = sfx[Random.Range(0, sfx.Length)];
            if (isAutoStart) GetComponent<AudioSource>().Play();
        }
    }
}
