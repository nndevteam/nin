﻿using UnityEngine;

public class SpriteRandom : MonoBehaviour {
    public Sprite[] imgs;
    void Start() {
        if (imgs != null && imgs.Length > 0) {
            GetComponent<SpriteRenderer>().sprite = imgs[Random.Range(0, imgs.Length)];
        }
    }
}
