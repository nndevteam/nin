﻿public class RangeHelper {
    public static bool inRange(float value, float from, float to) {
        return value >= from && value <= to;
    }
}
