﻿using UnityEngine;

public class ChildLayers : MonoBehaviour {
    public void setLayer(string layerName, int layerSortingLayer) {
        foreach (Transform child in transform) {
            if (child.gameObject.GetComponent<SpriteRenderer>() == null) continue;
            if (layerName != null) child.GetComponent<SpriteRenderer>().sortingLayerName = layerName;
            child.GetComponent<SpriteRenderer>().sortingOrder = child.GetComponent<SpriteRenderer>().sortingOrder + layerSortingLayer;
        }
    }
}
