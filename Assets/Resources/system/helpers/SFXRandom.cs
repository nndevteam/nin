﻿using UnityEngine;

public class SFXRandom : MonoBehaviour {
    public bool isAutoPlay = false;
    public AudioClip[] sfx;

    void Start() {
        if (isAutoPlay) onSound();
    }

    public void onSound() {
        if (! SavedSettings.getInstance().isSound()) {
            GetComponent<AudioSource>().Stop();
            GetComponent<AudioSource>().mute = true;
        } else if (sfx != null && sfx.Length > 0) {
            GetComponent<AudioSource>().mute = false;
            GetComponent<AudioSource>().clip = sfx[Random.Range(0, sfx.Length)];
            GetComponent<AudioSource>().Play();
        }
    }
}
