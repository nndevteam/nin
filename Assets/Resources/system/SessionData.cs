﻿public class SessionData {
    public static SessionData instance = null;

    private RandomLocationOptions randomLocationOptions;
    public static SessionData getInstance() {
        if (instance == null) {
            instance = new SessionData();
        }
        return instance;
    }

    public SessionData() {}

    public RandomLocationOptions getRandomLocationOptions() {
        return randomLocationOptions;
    }
    public void setRandomLocationOptions(RandomLocationOptions val) {
        randomLocationOptions = val;
    }
}
