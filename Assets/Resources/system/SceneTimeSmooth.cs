﻿using System.Collections;
using UnityEngine;

public class SceneTimeSmooth : MonoBehaviour {
    private bool isActionState = false;

    public void toAction() {
        if (!isActionState) {
            Time.timeScale = 1;
        }
        isActionState = true;
    }

    public void toIdle() {
        if (isActionState) {
            StartCoroutine(timeSmooth());
        }
        isActionState = false;
    }

    void FixedUpdate() {
        GameObject world = GameObject.FindGameObjectWithTag("World");
        bool isIdle = true;
        foreach (CharecterController sceneObj in world.GetComponentsInChildren<CharecterController>()) {
            if (sceneObj.getAI() != null && sceneObj.getAI().target != null && sceneObj.GetComponent<CharecterStats>().getHealth() > 0) {
                isIdle = false;
            }
        }

        if (isIdle) {
            toIdle();
        } else {
            toAction();
        }

        GameObject player = GameObject.FindGameObjectWithTag("Global").GetComponent<PlayerController>().getPlayerObj();
        if (player == null || player.GetComponent<CharecterStats>() == null || player.GetComponent<CharecterStats>().getHealth() <= 0) {
            toIdle();
        }
    }

    private IEnumerator timeSmooth() {
        Time.timeScale = 0.3f;
        yield return new WaitForSeconds(1.5f);
        Time.timeScale = 1;
    }
}
