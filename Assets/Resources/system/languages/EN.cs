﻿using System.Collections.Generic;

public class EN {
    private Dictionary<string, string> translationList = new Dictionary<string, string>();

    public EN() {
        translationList.Add("version", "1.0.0");
        translationList.Add("menu_title", "NIN");
        translationList.Add("menu_title_mini", "not invented name");
        translationList.Add("menu_title_var1", "you can play it");
        translationList.Add("menu_title_var2", "what could go like this");
        translationList.Add("menu_title_var3", "some kind of garbage");
        translationList.Add("menu_title_var4", "it is alive, it is alive");
        translationList.Add("menu_start", "Play!");
        translationList.Add("menu_exit", "No thanks");
        translationList.Add("menu_sound", "Sound (like sound)");
        translationList.Add("menu_music", "Music (if you can call it that)");
        translationList.Add("menu_blood", "Blood (paint it red)");
        translationList.Add("mission_exit", "Quit");
        translationList.Add("mission_floors_title", "Higher and higher. X floors of hell.");
        translationList.Add("mission_floors_description", "Increase the difficulty and pass all the challenges. \n\nAs difficulty increases, new weapons and armor are unlocked. \n\nAfter reaching difficulty 15, you will be able to independently set the parameters of the mission.");
        translationList.Add("mission_floors_difficult", "Difficult");
        translationList.Add("mission_floors_height", "Floors");
        translationList.Add("mission_floors_launch", " - ATTACK - ");
        translationList.Add("mission_floors_level_zero_title", " - ");
        translationList.Add("mission_floors_level_zero_txt", "Much easier");
        translationList.Add("mission_floors_level_prev_title", "Lowering the difficulty");
        translationList.Add("mission_floors_level_prev_txt", "Hurt?");
        translationList.Add("mission_floors_level_current_title", "The very thing");
        translationList.Add("mission_floors_level_current_txt", "Let's repeat");
        translationList.Add("mission_floors_level_next_title", "Increasing the difficulty");
        translationList.Add("mission_floors_level_next_txt", "Only forward");
        translationList.Add("mission_floors_level_max_title", "Custom difficulty");
        translationList.Add("mission_floors_level_max_txt", "");
        translationList.Add("mission_basement_title", "Deeper. To the very bottom.");
        translationList.Add("mission_basement_title_empty", "Soon or never...");
        translationList.Add("action_open", "open");
        translationList.Add("action_move", "enter");
        translationList.Add("action_cover", "cover");
        translationList.Add("action_pick_up", "pick up");
        translationList.Add("action_armor", "armor");
        translationList.Add("continue_uc", "CONTINUE");
        translationList.Add("control_hint_uc", "CONTROLS");
        translationList.Add("exit_uc", "END");
        translationList.Add("fail_uc", "FAIL");
        translationList.Add("complete_uc", "WIN");
        translationList.Add("hide_uc", "HIDE");
        translationList.Add("hide_forever_uc", "DON'T SHOW AGAIN");
        translationList.Add("control_hint_01", "A/D or Arrow left/Arrow right - move left/right");
        translationList.Add("control_hint_02", "Q + W/S or Q + Arrow up/Arrow down - cover up/down");
        translationList.Add("control_hint_03", "E - action");
        translationList.Add("control_hint_04", "1/2 - change weapon");
        translationList.Add("control_hint_05", "Space - Aim");
        translationList.Add("control_hint_06", "Ctrl or Left mouse - shoot");

        translationList.Add("mobile_control_hint_01", "Actions");
        translationList.Add("mobile_control_hint_02", "showing if available");
        translationList.Add("mobile_control_hint_03", "Covers top/bottom, one time actions");
        translationList.Add("mobile_control_hint_04", "Tap to shoot");
        translationList.Add("mobile_control_hint_05", "Tap slide move left/right");
        translationList.Add("mobile_control_hint_06", "Menu");
        //========================
    }

    public Dictionary<string, string> getTranslations() {
        return translationList;
    }
}
