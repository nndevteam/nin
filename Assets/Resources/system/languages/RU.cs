﻿using System.Collections.Generic;

public class RU {
    private Dictionary<string, string> translationList = new Dictionary<string, string>();

    public RU() {
        translationList.Add("version", "1.0.0"); 
        translationList.Add("menu_title", "НПН");
        translationList.Add("menu_title_mini", "не придуманное название");
        translationList.Add("menu_title_var1", "в это можно играть");
        translationList.Add("menu_title_var2", "что могло пойти так");
        translationList.Add("menu_title_var3", "такое себе");
        translationList.Add("menu_title_var4", "оно живое, оно живое");
        translationList.Add("menu_start", "Играем!");
        translationList.Add("menu_exit", "Нет, спасибо");
        translationList.Add("menu_sound", "Звук (какой никакой)");
        translationList.Add("menu_music", "Музыка (если это можно так назвать)");
        translationList.Add("menu_blood", "Кровь (покрасим в красный)");
        translationList.Add("mission_exit", "Выйти");
        translationList.Add("mission_floors_title", "Выше и выше. X этажей ада.");
        translationList.Add("mission_floors_description", "Повышай сложность и выдержи все испытания. \n\nПри повышении сложности открываются новые виды оружия и брони. \n\nПри достижении 15 сложности откроется возможность самостоятельно задавать параметры миссии.");
        translationList.Add("mission_floors_difficult", "Сложность");
        translationList.Add("mission_floors_height", "Этажей");
        translationList.Add("mission_floors_launch", " - В АТАКУ - ");
        translationList.Add("mission_floors_level_zero_title", " - ");
        translationList.Add("mission_floors_level_zero_txt", "Куда уж проще");
        translationList.Add("mission_floors_level_prev_title", "Понижаем сложность");
        translationList.Add("mission_floors_level_prev_txt", "Больно?");
        translationList.Add("mission_floors_level_current_title", "Самое оно");
        translationList.Add("mission_floors_level_current_txt", "Повторим");
        translationList.Add("mission_floors_level_next_title", "Повышаем сложность");
        translationList.Add("mission_floors_level_next_txt", "Только вперед");
        translationList.Add("mission_floors_level_max_title", "Своя сложность");
        translationList.Add("mission_floors_level_max_txt", "");
        translationList.Add("mission_basement_title", "Глубже. На самое дно.");
        translationList.Add("mission_basement_title_empty", "Скоро или никогда...");
        translationList.Add("action_open", "открыть");
        translationList.Add("action_move", "войти");
        translationList.Add("action_cover", "укрытие");
        translationList.Add("action_pick_up", "поднять");
        translationList.Add("action_armor", "броня");
        translationList.Add("continue_uc", "ПРОДОЛЖИТЬ");
        translationList.Add("control_hint_uc", "УПРАВЛЕНИЕ");
        translationList.Add("exit_uc", "ЗАВЕРШИТЬ");
        translationList.Add("fail_uc", "ПОТРАЧЕНО");
        translationList.Add("complete_uc", "ПОБЕДА");
        translationList.Add("hide_uc", "СКРЫТЬ");
        translationList.Add("hide_forever_uc", "БОЛЬШЕ НЕ ПОКЗЫВАТЬ");
        translationList.Add("control_hint_01", "A/D или Стрелка влево/Стрелка вправо - движение влево/вправо");
        translationList.Add("control_hint_02", "Q + W/S или Q + Стрелка вверх/Стрелка вниз - укрытие вверху/внизу");
        translationList.Add("control_hint_03", "E - действие");
        translationList.Add("control_hint_04", "1/2 - смена оружия");
        translationList.Add("control_hint_05", "Space - прицеливание");
        translationList.Add("control_hint_06", "Ctrl или ЛКМ - стрельба");


        translationList.Add("mobile_control_hint_01", "Действия");
        translationList.Add("mobile_control_hint_02", "показываются если доступны");
        translationList.Add("mobile_control_hint_03", "Укрытие снизу/сверху, разовые действия");
        translationList.Add("mobile_control_hint_04", "Стрельба");
        translationList.Add("mobile_control_hint_05", "Движение влево/вправо");
        translationList.Add("mobile_control_hint_06", "Меню"); 
        //========================
    }

    public Dictionary<string, string> getTranslations() {
        return translationList;
    }
}
