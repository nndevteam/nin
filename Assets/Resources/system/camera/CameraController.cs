﻿using UnityEngine;

public class CameraController : MonoBehaviour {
    private GameObject followObj;
    public float cameraFieldView = -20.0f;
    public float followSpeed = 4.0f;
    private float aimZoom = 0;
    void LateUpdate() {
        if (followObj == null) return;

        //Движение
        Vector3 newPosition = followObj.transform.position + (followObj.transform.right * (5 + (5 * aimZoom)));
        newPosition.z = cameraFieldView;
        transform.position = Vector3.Slerp(transform.position, newPosition, followSpeed * Time.deltaTime);
    }

    public void setAimZoom(float _aimZoom) {
        this.aimZoom = _aimZoom;
    }

    public void setFollowObj(GameObject obj) {
        followObj = obj;
    }
}
