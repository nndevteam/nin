﻿using UnityEngine;

public class CharecterController : MonoBehaviour {
    //=======MOVES
    public GameObject AI;
    public bool isPlayer = false;
    public float rotateSpeed = 6;
    public GameObject handIteract;
    public GameObject weaponHold1;
    public GameObject weaponHold2;

    private Animator animator = null;
    private float side = 0;

    //=======COVERS
    private Vector3 destination = Vector3.zero;
    private GameObject coverTop;
    private GameObject coverBottom;

    //=======WEAPONS
    private GameObject[] weapons = new GameObject[3];
    private GameObject activeWeapon = null;

    //=======ACTIONS
    private GameObject _availableAction;
    private bool availableControlls = true;
    void Start() {
        if (isPlayer) setIsPlayer();
        
        if (activeWeapon != null) {
            activeWeapon.GetComponent<FireArms>().setUser(gameObject);
            doAction("hands", activeWeapon.GetComponent<FireArms>().hands);
        }
        
    }

    public AI getAI() {
        AI returnAI = null;
        if (AI != null && AI.GetComponent<AI>() != null) {
            returnAI = AI.GetComponent<AI>();
        }
        return returnAI;
    }

    public void setIsPlayer() {
        isPlayer = true;
        if (AI != null) Destroy(AI);
        GameObject.FindGameObjectWithTag("Global").GetComponent<PlayerController>().setPlayerObj(gameObject);
    }

    public void unSetIsPlayer() {
        isPlayer = false;
        //GetComponent<AI>().enabled = false;
        //GameObject.FindGameObjectWithTag("Global").GetComponent<PlayerController>().setPlayerObj(gameObject);
    }


    void Update() {
        moveToDestination();

        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, side, 0), rotateSpeed * Time.deltaTime);

        if (_availableAction != null && isPlayer) _availableAction.GetComponent<ActionController>().onViewedAction(true);
    }

    public void moveBy(float moveDirection) {
        if (!isReady()) return;
        if (!availableControlls) return;
        if (GetComponent<CharecterController>().isAction("isCover")) return;
        
        doAction("move", moveDirection != 0);
        if (SavedSettings.getInstance().isSound()) {
            if (moveDirection != 0) {
                if (! GetComponent<AudioSource>().isPlaying) GetComponent<AudioSource>().Play();
            } else {
                GetComponent<AudioSource>().Stop();
            }
        }
        float activeSpeed = GetComponent<CharecterStats>().speed;
        if (animator.GetCurrentAnimatorStateInfo(0).IsTag("Aiming")) 
            activeSpeed = activeSpeed / 1.5f;
        

        float moveXPosition = transform.position.x + (moveDirection * 5);

        if (moveDirection > 0) {
            side = 0;
        } else if (moveDirection < 0) {
            side = 180;
        }
        
        transform.position = Vector3.MoveTowards(
            transform.position,
            new Vector3(moveXPosition, GetComponent<CharecterPositions>().getPosition(), 0),
            activeSpeed * Time.deltaTime
        );
    }

    public void setSide(float _side) {
        if (!availableControlls) return;
        side = _side;
    }

    public void moveToDestination() {
        if (destination == Vector3.zero) return;
        if (!isReady()) return;

        doAction("move", true);
        if (SavedSettings.getInstance().isSound()) {
            if (! GetComponent<AudioSource>().isPlaying) GetComponent<AudioSource>().Play();
        }
        float activeSpeed = GetComponent<CharecterStats>().speed;
        
        float moveDistance = activeSpeed * Time.deltaTime;

        /*
        if (transform.position.x < destination.x) {
            side = 0;
        } else if (transform.position.x > destination.x) {
            side = 180;
        }
        */

        transform.position = Vector3.MoveTowards(
            transform.position, 
            new Vector3(destination.x, destination.y, 0),
            moveDistance
        );

        if (transform.position == destination) {
            doAction("move", false);
            destination = Vector3.zero;
        }
    }

    public void replaceWeapon(GameObject weapon) {
        int weaponId = weapon.GetComponent<FireArms>().hands;
        weapons[weaponId] = weapon;

        if (activeWeapon != null && activeWeapon.GetComponent<FireArms>().hands == weapons[weaponId].GetComponent<FireArms>().hands) {
            Destroy(activeWeapon);
            activeWeapon = weapons[weaponId];
            activeWeapon.transform.parent = handIteract.transform;
            activeWeapon.transform.localPosition = Vector3.zero;
            activeWeapon.transform.localRotation = Quaternion.Euler(0, 0, 0);

            activeWeapon.GetComponent<FireArms>().setUser(gameObject);
            activeWeapon.GetComponent<FireArms>().setLayer(GetComponent<CharecterLayers>().getStartLayer() + 16);
        } else {
            if (weapons[weaponId].GetComponent<FireArms>().hands == 1) {
                foreach (Transform child in weaponHold1.transform) Destroy(child.gameObject);
                weapons[weaponId].transform.parent = weaponHold1.transform;
                weapons[weaponId].GetComponent<FireArms>().setLayer(GetComponent<CharecterLayers>().getStartLayer() + 15);
            } else {
                foreach (Transform child in weaponHold2.transform) Destroy(child.gameObject);
                weapons[weaponId].transform.parent = weaponHold2.transform;
                weapons[weaponId].GetComponent<FireArms>().setLayer(GetComponent<CharecterLayers>().getStartLayer() + 9);
            }

            weapons[weaponId].transform.localPosition = Vector3.zero;
            weapons[weaponId].transform.localRotation = Quaternion.Euler(0, 0, 0);
        }
        if (activeWeapon != null) doAction("hands", activeWeapon.GetComponent<FireArms>().hands);
        if (weaponId == 2) selectWeapon(2);
    }

    public void selectWeapon(int weaponId) {
        if (weaponId == -1 || weapons[weaponId] == null || !availableControlls) return;

        if (activeWeapon != null) {
            if (activeWeapon.GetComponent<FireArms>().hands == 1) {
                activeWeapon.transform.parent = weaponHold1.transform;
                activeWeapon.GetComponent<FireArms>().setLayer(GetComponent<CharecterLayers>().getStartLayer() + 15);
            } else {
                activeWeapon.transform.parent = weaponHold2.transform;
                activeWeapon.GetComponent<FireArms>().setLayer(GetComponent<CharecterLayers>().getStartLayer() + 9);
            }
            activeWeapon.transform.localPosition = Vector3.zero;
            activeWeapon.transform.localRotation = Quaternion.Euler(0, 0, 0);
        }

        activeWeapon = weapons[weaponId];
        activeWeapon.transform.parent = handIteract.transform;
        activeWeapon.transform.localPosition = Vector3.zero;
        activeWeapon.transform.localRotation = Quaternion.Euler(0, 0, 0);

        activeWeapon.GetComponent<FireArms>().setUser(gameObject);
        activeWeapon.GetComponent<FireArms>().setLayer(GetComponent<CharecterLayers>().getStartLayer() + 16);

        doAction("hands", activeWeapon.GetComponent<FireArms>().hands);
    }

    public void doAction(string action, bool value) {
        if (!isReady()) return;
        animator.SetBool(action, value);
    }

    public void doAction(string action, int value) {
        if (!isReady()) return;
        animator.SetInteger(action, value);
    }

    public bool isAction(string action) {
        if (!isReady()) return false;
        return animator.GetBool(action);
    }

    public void doAction(string action) {
        if (!isReady()) return;
        L.S("doAction:SetTrigger  " + action);
        animator.SetTrigger(action);
    }

    public void startAction() {
        if (!availableControlls) return;
        if (_availableAction == null) return;
        if (_availableAction.GetComponent<ActionController>().requiredSide != -1)
            side = _availableAction.GetComponent<ActionController>().requiredSide;

        doAction("ActionVerticalType", _availableAction.GetComponent<ActionController>().requiredActionVerticalType);

        if (_availableAction.GetComponent<ActionController>().onEndAnimation) {
            availableControlls = false;
            doAction("isAction", true);
            setHitAvailable(false);
        } else {
            _availableAction.GetComponent<ActionController>().activateAction(gameObject);
        }
    }

    public void endAction() {
        doAction("isAction", false);
        availableControlls = true;
        setHitAvailable(true);
        if (_availableAction == null) return;
        _availableAction.GetComponent<ActionController>().activateAction(gameObject);
    }

    public void setTarget(float target) {
        if (activeWeapon == null) return;
        string line = FireArms.CENTER_LAYER;
        if (target > 0) 
            line = FireArms.TOP_LAYER;
        
        if (target < 0) 
            line = FireArms.BOTTOM_LAYER;
        
        activeWeapon.GetComponent<FireArms>().setLine(line);
    }

    public void iteractWeapon(float pressing) {
        if (!isReady()) return;
        if (!availableControlls) return;
        if (!animator.GetCurrentAnimatorStateInfo(0).IsTag("Aim")) return;
        if (activeWeapon == null) return;
       
        activeWeapon.GetComponent<FireArms>().iteract(pressing, gameObject);
    }

    public GameObject getActiveWeapon() {
        return activeWeapon;
    }

    public void setLocationYPosition(float YPos) {
        GetComponent<CharecterPositions>().setPrefPosition(YPos);
        GetComponent<CharecterLayers>().updateLayerByPosition(GetComponent<CharecterPositions>().getPrefPosition());
    }

    public void coverAvailable(string line, GameObject cover) {
        L.S("coverAvailable::" + line);
        if (line.Equals(Constants.topCover)) 
            coverTop = cover;
        
        if (line.Equals(Constants.bottomCover))
            coverBottom = cover;
    }

    public void availableAction(GameObject action) {
        if (_availableAction != null && isPlayer) _availableAction.GetComponent<ActionController>().onViewedAction(false);
        _availableAction = action;
    }

    public GameObject getAvailableAction() {
        return _availableAction;
    }

    public void handleCover(float startCover) {
        L.S("MOVE - handleCover");
        if (!isReady()) return;
        if (!availableControlls) return;

        if (isAction("isCover")) {
            doAction("isCover", false);
            doAction("isCoverStand", false);
            doAction("isCoverSit", false);
            GetComponent<CharecterLayers>().updateLayerLine(CharecterLayers.CENTER_LAYER);
            GetComponent<CharecterLayers>().updateLayerByPosition(GetComponent<CharecterPositions>().getPrefPosition());
            //destination = new Vector3(transform.position.x, GetComponent<CharecterPositions>().getPosition(), 0);
        } else if (startCover != 0) {
            if (startCover > 0 && coverTop == null) return;
            if (startCover < 0 && coverBottom == null) return;
            
            GameObject activeCover = startCover > 0 ? coverTop : coverBottom;

            float coverYPos = 0;
            if (activeCover.GetComponent<CoverController>().line.Equals(Constants.topCover)) {
                GetComponent<CharecterLayers>().updateLayerLine(CharecterLayers.TOP_LAYER);
                coverYPos = GetComponent<CharecterPositions>().Ymax - 0.15f;
            } else {
                GetComponent<CharecterLayers>().updateLayerLine(CharecterLayers.BOTTOM_LAYER);
                coverYPos = GetComponent<CharecterPositions>().Ymin + 0.15f;
            }
            //doAction("move", false);
            doAction("isCover", true);
            doAction(activeCover.GetComponent<CoverController>().coverAnimation);
            doAction("is" + activeCover.GetComponent<CoverController>().coverAnimation, true);
            side = activeCover.GetComponent<CoverController>().side;
            if (isPlayer) activeCover.GetComponent<CoverController>().hideUI();

            GetComponent<CharecterLayers>().updateLayerByPosition(coverYPos);
            destination = new Vector3(
                activeCover.transform.position.x,
                GetComponent<CharecterPositions>().getBasePosition() + coverYPos, 
                0
            );
        }
    }

    public void setHitAvailable(bool available) {
        if (available) {
            GetComponent<CharecterSkin>().head.GetComponent<CircleCollider2D>().isTrigger = false;
            GetComponent<CharecterSkin>().torso.GetComponent<BoxCollider2D>().isTrigger = false;
            GetComponent<CharecterSkin>().bottom.GetComponent<BoxCollider2D>().isTrigger = false;
        } else {
            GetComponent<CharecterSkin>().head.GetComponent<CircleCollider2D>().isTrigger = true;
            GetComponent<CharecterSkin>().torso.GetComponent<BoxCollider2D>().isTrigger = true;
            GetComponent<CharecterSkin>().bottom.GetComponent<BoxCollider2D>().isTrigger = true;
        }
    }

    public void clearOnDead() {
        if (activeWeapon != null) activeWeapon.GetComponent<FireArms>().onLoseWeapon(transform.parent);
        if (GameObject.FindGameObjectWithTag("Global").GetComponent<LocationObjectiveDestroy>() != null) {
            if (isPlayer) {
                GameObject.FindGameObjectWithTag("Global").GetComponent<LocationObjectiveDestroy>().onFailed();
            } else {
                GameObject.FindGameObjectWithTag("Global").GetComponent<LocationObjectiveDestroy>().removeTarget(gameObject);
            }
        }
        if (AI != null) Destroy(AI);
        GetComponent<CharecterLayers>().clearLayer();
        Destroy(GetComponent<AudioSource>());
        Destroy(GetComponent<Rigidbody2D>());
        Destroy(GetComponent<PolygonCollider2D>());
        Destroy(GetComponent<CharecterPositions>());
        Destroy(GetComponent<CharecterStats>());
        Destroy(GetComponent<CharecterSkin>());
        Destroy(GetComponent<CharecterHitControler>());
        Destroy(GetComponent<CharecterController>());
        Destroy(GetComponent<CharecterLayers>());
    }

    public void setAvailableControlls(bool _availableControlls) {
        availableControlls = _availableControlls;
    }

    public bool isReady() {
        if (animator == null) {
            animator = GetComponent<Animator>();
        }
        return animator != null;
    }

    public Animator getAnimator() {
        return animator;
    }

}
