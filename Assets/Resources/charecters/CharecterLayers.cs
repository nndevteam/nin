﻿using UnityEngine;

public class CharecterLayers : MonoBehaviour {
    public const string TOP_LAYER = "CharecterTopPhisics";
    public const string CENTER_LAYER = "CharecterCenterPhisics";
    public const string BOTTOM_LAYER = "CharecterBottomPhisics";
    public int step = 20;
    private int startLayer = 0;
    private string activeLayer = CENTER_LAYER;

    void Start() {
        
    }

    public int getStartLayer() {
        return startLayer;
    }

    public void updateLayerByPosition(float preferedYPosition) {
        startLayer = 7100 - (int)(System.Math.Round(preferedYPosition, 1) * 1000);
        //pos = 5.1435345 = layer = 5100 + base
        L.S("CharecterLayers : updateLayerByPosition : " + startLayer);
        foreach (SpriteRenderer sp in gameObject.GetComponentsInChildren<SpriteRenderer>()) if (!sp.name.Equals("view") && sp.gameObject.GetComponent<LayerBaseConst>() != null) {
            int newLayer = sp.gameObject.GetComponent<LayerBaseConst>().baseLayer + startLayer;
            sp.sortingOrder = newLayer;
        }
        foreach (FireArms fa in gameObject.GetComponentsInChildren<FireArms>()) {
            if (fa.transform.parent.name.Equals("iteract")) fa.setLayer(startLayer + 16);
            if (fa.transform.parent.name.Equals("weapon_hold_1")) fa.setLayer(startLayer + 15);
            if (fa.transform.parent.name.Equals("weapon_hold_2")) fa.setLayer(startLayer + 9);
        }
    }
    

    public void clearLayer() {
        startLayer = 0;
        L.S("CharecterLayers : clearLayer");
        foreach (SpriteRenderer sp in gameObject.GetComponentsInChildren<SpriteRenderer>()) if (! sp.name.Equals("view")) {
            int newLayer = sp.gameObject.GetComponent<LayerBaseConst>().baseLayer;
            sp.sortingOrder = newLayer;
        }
        foreach (FireArms fa in gameObject.GetComponentsInChildren<FireArms>()) {
            if (fa.transform.parent.name.Equals("iteract")) fa.setLayer(startLayer + 16);
            if (fa.transform.parent.name.Equals("weapon_hold_1")) fa.setLayer(startLayer + 15);
            if (fa.transform.parent.name.Equals("weapon_hold_2")) fa.setLayer(startLayer + 9);
        }
    }

    public void updateLayerLine(string line) {
        activeLayer = line;
        gameObject.layer = LayerMask.NameToLayer(line);
        //head
        gameObject.transform.GetChild(0).gameObject.layer = LayerMask.NameToLayer(line);
        //torso
        gameObject.transform.GetChild(1).gameObject.layer = LayerMask.NameToLayer(line);
        //bottom
        gameObject.transform.GetChild(2).gameObject.layer = LayerMask.NameToLayer(line);
    }

    public string getActiveLayer() {
        return activeLayer;
    }
}
