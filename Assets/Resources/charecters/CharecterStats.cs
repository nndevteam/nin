﻿using UnityEngine;

public class CharecterStats : MonoBehaviour {
    public float health = 1;
    public float armor = 1;
    public float speed = 0.1f;

    public float maxHealth = 1;
    public float maxArmor = 1;

    private float timeRecover = 5f;
    private bool isHited = false;
    

    public void setHited() {
        isHited = true;
        timeRecover = 5f;
    }

    void FixedUpdate() {
        if (GetComponent<CharecterController>() == null || ! GetComponent<CharecterController>().isPlayer) return;
        if (isHited == true) {
            timeRecover = timeRecover - Time.fixedDeltaTime;
            if (timeRecover <= 0) isHited = false;
        } else if (health < maxHealth) {
            health += 20 * Time.fixedDeltaTime;
            if (health > maxHealth) health = maxHealth;
        }
    }

    public void bindHealth(float _health) {
        health = _health;
        maxHealth = _health;
    }

    public void bindArmor(float _armor) {
        armor = _armor;
        maxArmor = _armor;
    }

    public float getHealth() {
        return health;
    }

    public float getProcentHealth() {
        float pr1 = 1 / maxHealth;
        return health * pr1;
    }
    public float getProcentArmor() {
        float pr1 = 1 / maxArmor;
        return armor * pr1;
    }


    public float getArmor() {
        return armor;
    }

    public void updateHealth(float update) {
        health += update;
    }
    public void updateArmor(float update) {
        armor += update;
    }

}
