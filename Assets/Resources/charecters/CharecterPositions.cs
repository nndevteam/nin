﻿using UnityEngine;

public class CharecterPositions : MonoBehaviour {
    //Human
    public float Ymin = 3f;
    public float Ymax = 7f;

    public float defYmin = 4.4f;
    public float defYmax = 5.5f;
    public float defYcenter = 5.2f;
    //=====
    //3-7 all positions
    //center 5.2
    //4.4 - 5.5 center positions
    private float baseYPos = 0;
    private float preferedYPos = 0;

    void Start() {
        preferedYPos = defYcenter;
    }

    public float setPrefPosition(float baseY) {
        baseYPos = baseY;
        preferedYPos = Random.Range(defYmin, defYmax);
        return getPosition();
    }

    public float getBasePosition() {
        return baseYPos;
    }

    public float getPrefPosition() {
        return preferedYPos;
    }

    public float getPosition() {
        return baseYPos + preferedYPos;
    }
}
