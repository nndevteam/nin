﻿using UnityEngine;

public class CharecterSkin : MonoBehaviour {
    public GameObject head;

    public GameObject torso;
    public GameObject RArm;
    public GameObject RArm2;
    public GameObject LArm;
    public GameObject LArm2;

    public GameObject bottom;
    public GameObject RFoot;
    public GameObject RFoot2;
    public GameObject LFoot;
    public GameObject LFoot2;
    public GameObject iterator;

    private string _type;

    public void generateByType(string type, int targetDifficult) {
        this._type = type;
        CosmeticsRandomSystem crs = GameObject.FindGameObjectWithTag("CosmeticData").GetComponent<CosmeticsRandomSystem>();
        Sprite[] headView = crs.getRHead(this._type);
        //0 - base
        //1 - hair
        //2 - beard
        head.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = headView[0];
        head.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = headView[1];
        head.transform.GetChild(2).GetComponent<SpriteRenderer>().sprite = headView[2];
        //Glass
        //head.transform.GetChild(3).GetComponent<SpriteRenderer>().sprite = headView[2];
        
        GameObject torsoView = crs.getRTorso(this._type);
        torso.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = torsoView.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite;
        RArm.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = torsoView.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite;
        RArm2.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = torsoView.transform.GetChild(2).GetComponent<SpriteRenderer>().sprite;
        LArm.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = torsoView.transform.GetChild(3).GetComponent<SpriteRenderer>().sprite;
        LArm2.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = torsoView.transform.GetChild(4).GetComponent<SpriteRenderer>().sprite;

        GameObject bottomView = crs.getRBottom(this._type);
        bottom.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = bottomView.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite;
        RFoot.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = bottomView.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite;
        RFoot2.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = bottomView.transform.GetChild(2).GetComponent<SpriteRenderer>().sprite;
        LFoot.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = bottomView.transform.GetChild(3).GetComponent<SpriteRenderer>().sprite;
        LFoot2.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = bottomView.transform.GetChild(4).GetComponent<SpriteRenderer>().sprite;

        bindArmor(crs, targetDifficult);
    }

    public void setArmorByType(string type, int targetDifficult) {
        this._type = type;
        bindArmor(
            GameObject.FindGameObjectWithTag("CosmeticData").GetComponent<CosmeticsRandomSystem>(),
            targetDifficult
        );
    }
    
    public void clearArmor() {
        if (head.transform.GetChild(4).GetComponent<SpriteRenderer>().sprite != null) {
            //RETURN HAIRS
            Sprite[] headView  = GameObject.FindGameObjectWithTag("CosmeticData").GetComponent<CosmeticsRandomSystem>().getRHead(this._type); ;
            head.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = headView[1];
        }
        head.transform.GetChild(4).GetComponent<SpriteRenderer>().sprite = null;

        Color transparentArmor = torso.transform.GetChild(1).GetComponent<SpriteRenderer>().color;
        transparentArmor.a = 0.5f;
        torso.transform.GetChild(1).GetComponent<SpriteRenderer>().color = transparentArmor;

        RArm.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = null;
        RArm2.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = null;
        LArm.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = null;
        LArm2.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = null;

        bottom.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = null;
        RFoot.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = null;
        RFoot2.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = null;
        LFoot.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = null;
        LFoot2.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = null;

    }

    private void bindArmor(CosmeticsRandomSystem crs, int targetDifficult) {
        //Armor & Caps
        head.transform.GetChild(4).GetComponent<SpriteRenderer>().sprite = crs.getRHeadWear(this._type);
        if (head.transform.GetChild(4).GetComponent<SpriteRenderer>().sprite != null) {
            //REMOVE HAIRS IF HELMET
            head.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = null;
        }

        int armorValue = -1000;
        int chance = Random.Range(1, 101);

        if (targetDifficult == 4 || targetDifficult == 5 || targetDifficult == 6) {
            //+BODY ARMOR%
            GameObject armorTorso = crs.getArmorTorso(CosmeticsRandomSystem.TYPE_ARMOR_LIGHT);
            if (armorTorso != null && chance > 50) {
                armorValue = 50;
                torso.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = armorTorso.GetComponent<SpriteRenderer>().sprite;
            }
        }
        if (targetDifficult == 7 || targetDifficult == 8 || targetDifficult == 9) {
            //+BODY ARMOR
            //+FOOT ARMOR%
            GameObject armorTorso = crs.getArmorTorso(CosmeticsRandomSystem.TYPE_ARMOR_LIGHT);
            if (armorTorso != null) {
                armorValue = 50;
                torso.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = armorTorso.GetComponent<SpriteRenderer>().sprite;
            }
            GameObject armorBottom = crs.getArmorBottom(CosmeticsRandomSystem.TYPE_ARMOR_LIGHT);
            if (armorBottom != null && chance > 50) {
                armorValue = armorValue + 50;
                RFoot2.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = armorBottom.GetComponent<SpriteRenderer>().sprite;
                LFoot2.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = armorBottom.GetComponent<SpriteRenderer>().sprite;
            }
        }
        if (targetDifficult == 10 || targetDifficult == 11 || targetDifficult == 12) {
            //+BODY ARMOR++
            //+FOOT ARMOR++
            GameObject armorTorso = crs.getArmorTorso(CosmeticsRandomSystem.TYPE_ARMOR_HEAVY);
            if (armorTorso != null) {
                armorValue = 120;
                torso.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = armorTorso.GetComponent<SpriteRenderer>().sprite;
            }
            GameObject armorBottom = crs.getArmorBottom(CosmeticsRandomSystem.TYPE_ARMOR_HEAVY);
            if (armorBottom != null) {
                armorValue = armorValue + 80;
                RFoot2.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = armorBottom.GetComponent<SpriteRenderer>().sprite;
                LFoot2.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = armorBottom.GetComponent<SpriteRenderer>().sprite;
            }
        }
        if (targetDifficult == 13 || targetDifficult == 14 || targetDifficult == 15) {
            //+HEAD ARMOR%
            //+BODY ARMOR++
            //+FOOT ARMOR++
            GameObject armorTorso = crs.getArmorTorso(CosmeticsRandomSystem.TYPE_ARMOR_HEAVY);
            if (armorTorso != null) {
                armorValue = 120;
                torso.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = armorTorso.GetComponent<SpriteRenderer>().sprite;
            }
            GameObject armorBottom = crs.getArmorBottom(CosmeticsRandomSystem.TYPE_ARMOR_HEAVY);
            if (armorBottom != null) {
                armorValue = armorValue + 80;
                RFoot2.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = armorBottom.GetComponent<SpriteRenderer>().sprite;
                LFoot2.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = armorBottom.GetComponent<SpriteRenderer>().sprite;
            }
            GameObject armorHead = crs.getArmorHead(CosmeticsRandomSystem.TYPE_ARMOR_LIGHT);
            if (armorHead != null && chance > 50) {
                armorValue = armorValue + 50;
                head.transform.GetChild(4).GetComponent<SpriteRenderer>().sprite = armorHead.GetComponent<SpriteRenderer>().sprite;
            }
        }
        if (targetDifficult >= 16) {
            //+HEAD ARMOR++
            //+BODY ARMOR++
            //+FOOT ARMOR++
            GameObject armorTorso = crs.getArmorTorso(CosmeticsRandomSystem.TYPE_ARMOR_HEAVY);
            if (armorTorso != null) {
                armorValue = 120;
                torso.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = armorTorso.GetComponent<SpriteRenderer>().sprite;
            }
            GameObject armorBottom = crs.getArmorBottom(CosmeticsRandomSystem.TYPE_ARMOR_HEAVY);
            if (armorBottom != null) {
                armorValue = armorValue + 80;
                RFoot2.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = armorBottom.GetComponent<SpriteRenderer>().sprite;
                LFoot2.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = armorBottom.GetComponent<SpriteRenderer>().sprite;
            }
            GameObject armorHead = crs.getArmorHead(CosmeticsRandomSystem.TYPE_ARMOR_HEAVY);
            if (armorHead != null) {
                armorValue = armorValue + 100;
                head.transform.GetChild(4).GetComponent<SpriteRenderer>().sprite = armorHead.GetComponent<SpriteRenderer>().sprite;
            }
        }
        GetComponent<CharecterStats>().bindHealth(100 + (targetDifficult * 10));
        GetComponent<CharecterStats>().bindArmor(armorValue + (targetDifficult * 10));
    }
}
