﻿using System.Collections;
using UnityEngine;

public class CharecterHitControler : MonoBehaviour {
    public static string FX_TYPE_BLOOD = "BLOOD";
    public static string FX_TYPE_ARMOR = "ARMOR";

    public GameObject[] fxBlood;
    public GameObject[] fxArmor;

    public CircleCollider2D head;
    public BoxCollider2D torso;
    public BoxCollider2D bottom;

    public string setHit(Collision2D hit, float damage) {
        string fxType = FX_TYPE_BLOOD;
        if (GetComponent<CharecterStats>().getHealth() <= 0) return fxType;
        
        string position = "HitBottom";
        if (hit.collider.Equals(head)) position = "HitHead";
        if (hit.collider.Equals(torso)) position = "HitCenter";

        float factorDamage = 0.5f;
        //Head
        if (position.Equals("HitHead")) factorDamage = 2f;
        //Torso
        if (position.Equals("HitCenter")) factorDamage = 1f;
        
        L.S("HIT - " + position + " :" + (damage * factorDamage));
        if (GetComponent<CharecterStats>().getArmor() > 0) {
            fxType = FX_TYPE_ARMOR;
            GetComponent<CharecterStats>().updateArmor(-1 * damage);
            if (GetComponent<CharecterStats>().getArmor() <= 0) GetComponent<CharecterSkin>().clearArmor();
        } else {
            fxType = FX_TYPE_BLOOD;
            GetComponent<CharecterStats>().updateHealth(-1 * damage * factorDamage);
        }

        if (GetComponent<CharecterStats>().getHealth() <= 0) {
            GetComponent<CharecterController>().moveBy(0);
            GetComponent<CharecterController>().setAvailableControlls(false);
            GetComponent<CharecterController>().doAction("dead", true);

            if (position.Equals("HitHead")) {
                //Head
                GetComponent<CharecterController>().doAction("deadHead");
                StartCoroutine(onDead("deadHead"));
            } else if (position.Equals("HitCenter")){
                //Torso
                GetComponent<CharecterController>().doAction("deadTop");
                StartCoroutine(onDead("deadTop"));
            } else {
                //Bottom
                GetComponent<CharecterController>().doAction("deadBottom");
                StartCoroutine(onDead("deadBottom"));
            }
        } else {
            GetComponent<CharecterStats>().setHited();
            GetComponent<CharecterController>().doAction("Hit");
            if (position.Equals("HitHead")) {
                //Head
                GetComponent<CharecterController>().doAction("HeadHit");
            } else if (position.Equals("HitCenter")) {
                //Torso
                GetComponent<CharecterController>().doAction("TopHit");
            } else {
                //Bottom
                GetComponent<CharecterController>().doAction("BottomHit");
            }
        }
        return fxType;
    }

    public GameObject getEffect(string type) {
        GameObject _fx = null;
        GameObject[] listFx = fxBlood;
        if (type.Equals(FX_TYPE_ARMOR)) listFx = fxArmor;
        
        if (listFx != null && listFx.Length > 0) {
            _fx = listFx[Random.Range(0, listFx.Length)];
        }
        return _fx;
    }

    IEnumerator onDead(string finalize) {
        while (GetComponent<CharecterController>() != null) {
            yield return new WaitForSeconds(0.1f);
            if (GetComponent<CharecterController>() != null) GetComponent<CharecterController>().doAction(finalize);
        }
    }
}
