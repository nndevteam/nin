﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI : MonoBehaviour {
    public GameObject target;

    public List<GameObject> groupInView = new List<GameObject>();

    private GameObject charecter;
    private Vector3 destination = Vector3.zero;
    private float isMoveBy = 0; 
    private bool isAiming = false;
    private bool isShooting = false;


    private float minMoveReaction;
    private float minWaitReaction;

    private float minAimReaction;
    private float maxFireReaction;
    private float minAfterFireReaction;

    private int difficult = 0;
    private bool wasPaused = false;

    void OnDisable() {
        //StopAllCoroutines();
        //wasPaused = true;
    }

    void OnEnable() {
        //if (wasPaused) StartCoroutine(makeDecisions());
    }

    void Start() {
        charecter = transform.parent.gameObject;
        RandomLocationOptions randomLocationOptions = null;
        if (GameObject.FindGameObjectWithTag("Global").GetComponent<LocationObjectiveDestroy>() != null) {
            randomLocationOptions = GameObject.FindGameObjectWithTag("Global").GetComponent<LocationObjectiveDestroy>().getRandomLocationOptions();
        }
        if (randomLocationOptions == null || charecter == null) return;

        difficult = randomLocationOptions.difficult;

        //MOVE/WAIT
        minMoveReaction = 0.3f;
        minWaitReaction = 1f - (difficult / 10);
        if (minWaitReaction < 0) minWaitReaction = 0;
        //=========
        //FIRE_REACTION
        minAimReaction = 1 - (difficult / 10);
        if (minAimReaction < 0) minAimReaction = 0;
        maxFireReaction = 0.5f + (difficult * 2 / 10);
        minAfterFireReaction = 1.75f - (difficult / 10);
        if (minAfterFireReaction < 0) minAfterFireReaction = 0;
        //=========

        StartCoroutine(makeDecisions());
    }

    void Update() {
        charecter.GetComponent<CharecterController>().moveBy(isMoveBy);
        if (target == null || target.GetComponent<CharecterController>() == null || charecter == null || charecter.GetComponent<CharecterController>() == null) return;
        charecter.GetComponent<CharecterController>().doAction("aim", isAiming);
        if (isShooting) {
            charecter.GetComponent<CharecterController>().iteractWeapon(0);
            charecter.GetComponent<CharecterController>().iteractWeapon(1);
        }
    }

    void FixedUpdate() {
        if (target == null || target.GetComponent<CharecterController>() == null || charecter == null || charecter.GetComponent<CharecterController>() == null) return;
        
        switch (target.GetComponent<CharecterLayers>().getActiveLayer()) {
            case CharecterLayers.TOP_LAYER:
                charecter.GetComponent<CharecterController>().setTarget(1);
                break;

            case CharecterLayers.CENTER_LAYER:
                charecter.GetComponent<CharecterController>().setTarget(0);
                break;

            case CharecterLayers.BOTTOM_LAYER:
                charecter.GetComponent<CharecterController>().setTarget(-1);
                break;
            default:
                charecter.GetComponent<CharecterController>().setTarget(0);
                break;
        }

        foreach (GameObject obj in groupInView) {
            if (obj.GetComponent<CharecterController>() == null) {
                groupInView.Remove(obj);
                break;
            } else if (obj.GetComponent<CharecterController>().getAI() != null) {
                obj.GetComponent<CharecterController>().getAI().setTarget(target);
            }
        }
    }

    void OnTriggerEnter2D(Collider2D col) {
        GameObject inView = col.gameObject.transform.parent.gameObject;
        if (inView.GetComponent<CharecterController>() != null) {
            if (!inView.GetComponent<CharecterController>().isPlayer && !groupInView.Contains(inView)) {
                groupInView.Add(inView);
            } else if (inView.GetComponent<CharecterController>().isPlayer) {
                setTarget(inView);
            }
        }
    }

    void OnTriggerExit2D(Collider2D col) {
        if (col.gameObject.transform.parent.GetComponent<CharecterController>() != null) {
            groupInView.Remove(col.gameObject.transform.parent.gameObject);
        }
    }

    public List<GameObject> getGroupInView() {
        return groupInView;
    }

    public void setTarget(GameObject target) {
        if (this.target == null && target.GetComponent<CharecterController>() != null && target.GetComponent<CharecterController>().isPlayer) this.target = target;
    }

    private IEnumerator makeDecisions() {
        //ПОКОЙ
        while (charecter == null || !charecter.GetComponent<CharecterController>().isReady()) {
            yield return new WaitForSeconds(1f);
        }
        //РЕШЕНИЯ
        while (true) {
            if (target == null || target.GetComponent<CharecterController>() == null) {
                destination = Vector3.zero;
                yield return AI_wait();
                continue;
            }
            float distance = Vector3.Distance(charecter.transform.position, target.transform.position);
            float direction = charecter.transform.position.x - target.transform.position.x;

            L.S("distance - " + distance);
            L.S("direction - " + (direction < 0 ? "right" : "left"));
            if (charecter.GetComponent<CharecterController>().isAction("isCover") && !isAiming) {
                L.S("AIM::minReaction - " + minAimReaction);
                yield return AI_aiming();
            }
            if (isShooting) {
                isAiming = false;
                isShooting = false;
                L.S("STOP SHOT");
                yield return new WaitForSeconds(Random.Range(minAfterFireReaction, minAfterFireReaction + 0.25f));
            }
            if (isAiming) {
                isShooting = true;
                L.S("SHOT::maxFireReaction - " + maxFireReaction);
                yield return new WaitForSeconds(Random.Range(0.5f, maxFireReaction));
            }
            
            charecter.GetComponent<CharecterController>().setSide(direction < 0 ? 0 : 180);
            int chance = Random.Range(0, 100);
            if (distance <= 10) {
                // 10--  очень близко: обход \ стельба
                if (chance < 50) {
                    L.S("DISTANCE::MIN - FIRE");
                    yield return AI_aiming();
                } else {
                    L.S("DISTANCE::MIN - MOVE BACK");
                    yield return AI_move(direction < 0 ? 1 : -1);
                }
            }
            if (distance <= 20) {
                // 20--  близко: стрельба \ удлинение дистанции
                if (chance < 50) {
                    L.S("DISTANCE::MIN - FIRE");
                    yield return AI_aiming();
                } else {
                    L.S("DISTANCE::MIN - MOVE AWAY");
                    yield return AI_move(direction < 0 ? -1 : 1);
                }
            } else if (distance > 20 && distance <= 35) {
                // 20-35 средне: перемещение к укрытию \ нет укрытия стрельба
                if (chance < 70) {
                    L.S("DISTANCE::MEDIUM - FIRE");
                    yield return AI_aiming();
                } else if (chance < 90) {
                    L.S("DISTANCE::MEDIUM - MOVE AWAY");
                    yield return AI_move(direction < 0 ? -1 : 1);
                } else {
                    L.S("DISTANCE::MEDIUM - WAIT");
                    yield return AI_wait();
                }
            } else if (distance > 35 && distance < 45) {
                // 35-45 дальняя: перемещение к укрытию \ сокращение дистанции \ нет укрытия стрельба 
                if (chance < 80) {
                    L.S("DISTANCE::LONG - FIRE");
                    yield return AI_aiming();
                } else if (chance < 90) {
                    L.S("DISTANCE::LONG - MOVE TO");
                    yield return AI_move(direction < 0 ? 1 : -1);
                } else {
                    L.S("DISTANCE::LONG - WAIT");
                    yield return AI_wait();
                }
            } else {
                // 45++  дальняя: сокращение дистанции
                L.S("DISTANCE::LONGEST - MOVE TO");
                yield return AI_move(direction < 0 ? 1 : -1);
            }

            charecter.GetComponent<CharecterController>().setSide(direction < 0 ? 0 : 180);
        }
    }

    private WaitForSeconds AI_wait() {
        isMoveBy = 0;
        return new WaitForSeconds(Random.Range(minWaitReaction, minWaitReaction + 0.5f));
    }

    private WaitForSeconds AI_move(int direction) {
        isMoveBy = direction;
        return new WaitForSeconds(Random.Range(minMoveReaction, minMoveReaction + 0.5f));
    }

    private WaitForSeconds AI_aiming() {
        isMoveBy = 0;
        isAiming = true;
        return new WaitForSeconds(Random.Range(minAimReaction, minAimReaction + 0.25f));
    }
}
