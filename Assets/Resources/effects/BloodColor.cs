﻿using UnityEngine;

public class BloodColor : MonoBehaviour {
    public Color specColor;

    void Start() {
        if (specColor != null && SavedSettings.getInstance().isBloody()) {
            GetComponent<SpriteRenderer>().color = specColor;
        }
    }
}
