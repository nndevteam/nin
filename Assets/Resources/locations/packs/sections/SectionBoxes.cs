﻿public class SectionBoxes {
    public static int x1 = 4;
    public static int x2 = 11;
    public static int x3 = 22;

    public static int getVarinatsByX(int x) {
        if (x == 1) return x1;
        if (x == 2) return x2;
        if (x == 3) return x3;
        return 1;
    }
}
