﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneAI01 : MonoBehaviour {
    public GameObject ch1;
    public GameObject ch2;

    public GameObject cover1;
    public GameObject cover2;

    private bool isShooting1 = false;
    private bool isShooting2 = false;

    void Start() {
        int difficult1 = Random.Range(1, 11);
        ch1.GetComponent<CharecterSkin>().generateByType(CosmeticsRandomSystem.TYPE_MILITARY, difficult1);

        GameObject.FindGameObjectWithTag("WeaponData").GetComponent<WeaponsRandomSystem>().prepareByDifficult(difficult1);
        GameObject weapon1 = Instantiate(GameObject.FindGameObjectWithTag("WeaponData").GetComponent<WeaponsRandomSystem>().getSecondWeapon());
        weapon1.GetComponent<FireArms>().ammoCount = int.MaxValue;
        weapon1.GetComponent<SFXRandom>().sfx = null;
        weapon1.GetComponent<WeaponSFX>().reloadSFX = null;
        ch1.GetComponent<CharecterController>().replaceWeapon(weapon1);
        ch1.GetComponent<CharecterController>().selectWeapon(2);
        
        int difficult2 = Random.Range(1, 11);
        ch2.GetComponent<CharecterSkin>().generateByType(CosmeticsRandomSystem.TYPE_MILITARY, difficult2);

        GameObject.FindGameObjectWithTag("WeaponData").GetComponent<WeaponsRandomSystem>().prepareByDifficult(difficult2);
        GameObject weapon2 = Instantiate(GameObject.FindGameObjectWithTag("WeaponData").GetComponent<WeaponsRandomSystem>().getSecondWeapon());
        weapon2.GetComponent<FireArms>().ammoCount = int.MaxValue;
        weapon2.GetComponent<SFXRandom>().sfx = null;
        weapon2.GetComponent<WeaponSFX>().reloadSFX = null;
        ch2.GetComponent<CharecterController>().replaceWeapon(weapon2);
        ch2.GetComponent<CharecterController>().selectWeapon(2);

        ch1.GetComponent<CharecterController>().coverAvailable(Constants.bottomCover, cover1);
        ch2.GetComponent<CharecterController>().coverAvailable(Constants.bottomCover, cover2);

        StartCoroutine(operateDemoCharecter(ch1));
        StartCoroutine(operateDemoCharecter(ch2));
    }

    void Update() {
        if (ch1.GetComponent<CharecterController>().isReady()) {
            ch1.GetComponent<CharecterController>().moveBy(0);
            if (isShooting1) {
                ch1.GetComponent<CharecterController>().doAction("aim", true);
                ch1.GetComponent<CharecterController>().iteractWeapon(1);
            } else {
                ch1.GetComponent<CharecterController>().doAction("aim", false);
            }
        }
        if (ch2.GetComponent<CharecterController>().isReady()) {
            ch2.GetComponent<CharecterController>().moveBy(0);
            if (isShooting2) {
                ch2.GetComponent<CharecterController>().doAction("aim", true);
                ch2.GetComponent<CharecterController>().iteractWeapon(1);
            } else {
                ch2.GetComponent<CharecterController>().doAction("aim", false);
            }
        }
    }

    private IEnumerator operateDemoCharecter(GameObject obj) {
        while (!obj.GetComponent<CharecterController>().isReady()) {
            yield return new WaitForSeconds(1f);
        }

        while (!obj.GetComponent<CharecterController>().isAction("isCover")) {
            obj.GetComponent<CharecterController>().handleCover(-1);
            yield return new WaitForSeconds(2f);
        }

        while (true) {
            if (!obj.GetComponent<CharecterController>().getAnimator().GetCurrentAnimatorStateInfo(0).IsTag("Aim")) {
                if (obj == ch1) {
                    isShooting1 = true;
                } else {
                    isShooting2 = true;
                }
            } else {
                if (obj == ch1) {
                    isShooting1 = false;
                } else {
                    isShooting2 = false;
                }
            }

            yield return new WaitForSeconds(Random.Range(1, 4));
        }
    }
}
