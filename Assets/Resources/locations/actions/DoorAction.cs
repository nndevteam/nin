﻿using UnityEngine;

public class DoorAction : MonoBehaviour {
    public GameObject[] bounds;
    public GameObject openState;
    public GameObject closeState;
    public bool isOpened = false;

    void Start() {
        
    }

    public void activate() {
        GetComponent<SFXRandom>().onSound();
        isOpened = !isOpened;
        if (isOpened == true){
            openState.SetActive(true);
            closeState.SetActive(false);
        } else {
            openState.SetActive(false);
            closeState.SetActive(true);
        }
        foreach (GameObject b in bounds) {
            if (isOpened == true) { 
                b.SetActive(false);
            } else {
                b.SetActive(true);
            }
        }
    }
}
