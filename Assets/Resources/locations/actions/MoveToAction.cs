﻿using System.Collections.Generic;
using UnityEngine;

public class MoveToAction : MonoBehaviour {
    private Vector3 destination;
    private GameObject moveObject;
    private float alpha;
    private float speed = 2;
    private List<float> defaultAlpha = new List<float>();
    private List<SpriteRenderer> sprites = new List<SpriteRenderer>();

    void Start() {
        Destroy(GetComponent<SpriteRenderer>());
        destination = new Vector3(transform.position.x, transform.position.y, 0);
    }

    void Update() {
        if (moveObject == null || moveObject.GetComponent<CharecterController>() == null) return;
        if (moveObject.transform.position != destination && alpha != 0) { 
            alpha -= Time.deltaTime * speed;
            if (alpha < 0) alpha = 0;
            
            for (int i=0; i < sprites.Count; i++) {
                float _alpha = alpha;
                Color c = sprites[i].color;
                if (defaultAlpha[i] < _alpha) _alpha = defaultAlpha[i];
                c.a = _alpha;
                sprites[i].color = c;
            }
        }

        if (moveObject.transform.position != destination && alpha == 0) {
            float moveDistance = moveObject.GetComponent<CharecterStats>().speed * Time.deltaTime;
            moveObject.transform.position = Vector3.MoveTowards(
                moveObject.transform.position,
                destination,
                moveDistance * speed
            );
        }

        if (moveObject.transform.position == destination && alpha != 1) {
            alpha += Time.deltaTime * speed;
            if (alpha > 1) alpha = 1;
            
            for (int i = 0; i < sprites.Count; i++) {
                float _alpha = alpha;
                Color c = sprites[i].color;
                if (defaultAlpha[i] < _alpha) _alpha = defaultAlpha[i];
                c.a = _alpha;
                sprites[i].color = c;
            }
        }

        if (moveObject.transform.position == destination && alpha == 1) {
            moveObject.GetComponent<CharecterController>().setAvailableControlls(true);
            moveObject.GetComponent<CharecterController>().setHitAvailable(true);
            moveObject = null;
            defaultAlpha = new List<float>();
            sprites = new List<SpriteRenderer>();
        }
    }

    public void activate(GameObject executor) {
        if (executor.GetComponent<CharecterController>() == null) return;
        moveObject = executor;
        moveObject.GetComponent<CharecterController>().setAvailableControlls(false);
        moveObject.GetComponent<CharecterController>().setHitAvailable(false);

        sprites.AddRange(moveObject.GetComponentsInChildren<SpriteRenderer>());
        for (int i = 0; i < sprites.Count; i++) 
            defaultAlpha.Add(sprites[i].color.a);
        
        alpha = 1;
    }
}
