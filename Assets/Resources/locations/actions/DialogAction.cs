﻿using UnityEngine;

public class DialogAction : MonoBehaviour {
    public GameObject dialogViewTemplate;
    public string[] dialogs;
    public float[] timeToRead;
    public GameObject parent;

    private GameObject activeDialog;
    void Start() {
        Destroy(GetComponent<SpriteRenderer>()); 
    }

    public void activate(GameObject _parent) {
        if (activeDialog != null) return;
        activeDialog = Instantiate(
              dialogViewTemplate,
              new Vector3(0, 0, 0),
              GameObject.FindGameObjectWithTag("World").transform.rotation,
              GameObject.FindGameObjectWithTag("World").transform
        );
        if (this.parent != null) {
            activeDialog.GetComponent<DialogController>().setParent(this.parent, transform.position);
        } else {
            activeDialog.GetComponent<DialogController>().setParent(_parent, transform.position);
        }

        activeDialog.GetComponent<DialogController>().setText(
            dialogs,
            timeToRead
        );
    }
}
