﻿using UnityEngine;

public class BoxArmorAction : MonoBehaviour {
    public Sprite emptyState;

    public void activate(GameObject executor) {
        if (executor.GetComponent<CharecterController>() == null) return;
        if (GameObject.FindGameObjectWithTag("Global").GetComponent<LocationObjectiveDestroy>() != null) {
            RandomLocationOptions rlo = GameObject.FindGameObjectWithTag("Global").GetComponent<LocationObjectiveDestroy>().getRandomLocationOptions();
            GetComponent<SpriteRenderer>().sprite = emptyState;
            executor.GetComponent<CharecterSkin>().setArmorByType(CosmeticsRandomSystem.TYPE_MILITARY, rlo.difficult + 5);
        }
    }
}
