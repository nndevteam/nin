﻿using UnityEngine;

public class BoxAction : MonoBehaviour {
    public GameObject openState;
    public GameObject closeState;
    public bool isOpened = false;
    void Start() {

    }

    public void activate() {
        isOpened = !isOpened;
        if (isOpened == true) {
            if (openState != null) openState.SetActive(true);
            if (closeState != null) closeState.SetActive(false);
        } else {
            if (openState != null) openState.SetActive(false);
            if (closeState != null) closeState.SetActive(true);
        }
    }
}
