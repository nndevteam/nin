﻿using UnityEngine;

public class CoverController : MonoBehaviour {
    public GameObject ui;
    public string line = Constants.topCover;
    public float side = 0;
    public string coverAnimation = "default";
    public bool isAutoHideUI = true;

    private MobileController mobileController = null;

    void Start() {
        if (Constants.isMobile) ui.SetActive(false);
        if (isAutoHideUI) ui.SetActive(false);
        if (GameObject.FindGameObjectWithTag("MobileGlobal") != null)
            mobileController = GameObject.FindGameObjectWithTag("MobileGlobal").GetComponent<MobileController>();
    }

    public void hideUI() {
        if (Constants.isMobile && mobileController != null) {
            if (line.Equals(Constants.topCover))
                mobileController.onShowHideCoverTopUI(false);

            if (line.Equals(Constants.bottomCover))
                mobileController.onShowHideCoverBottomUI(false);
        } else {
            ui.SetActive(false);
        }
    }

    void OnTriggerEnter2D(Collider2D col) {
        if (col.gameObject.transform.parent.GetComponent<CharecterController>() != null) {
            col.gameObject.transform.parent.GetComponent<CharecterController>().coverAvailable(line, gameObject);

            if (col.gameObject.transform.parent.GetComponent<CharecterController>().isPlayer) {
                if (Constants.isMobile && mobileController != null) {
                    if (line.Equals(Constants.topCover))
                        mobileController.onShowHideCoverTopUI(true);

                    if (line.Equals(Constants.bottomCover))
                        mobileController.onShowHideCoverBottomUI(true);
                } else {
                    ui.SetActive(true);
                }
            }
        }
    }

    void OnTriggerExit2D(Collider2D col) {
        if (col.gameObject.transform.parent.GetComponent<CharecterController>() != null) {
            col.gameObject.transform.parent.GetComponent<CharecterController>().coverAvailable(line, null);

            if (col.gameObject.transform.parent.GetComponent<CharecterController>().isPlayer) {
                if (Constants.isMobile && mobileController != null) {
                    if (line.Equals(Constants.topCover))
                        mobileController.onShowHideCoverTopUI(false);

                    if (line.Equals(Constants.bottomCover))
                        mobileController.onShowHideCoverBottomUI(false);
                } else {
                    ui.SetActive(false);
                }
            }
        }
    }
}
