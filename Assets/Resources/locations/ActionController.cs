﻿using TMPro;
using UnityEngine;

public class ActionController : MonoBehaviour {
    public bool isTrigger = false;
    public bool onEndAnimation = false;
    public bool onActivationDestroy = false;
    public int requiredSide = -1;
    public int requiredActionVerticalType = 1;
    public GameObject uiIcon;
    public string hintStringKey;
    public string iteractKey;
    public GameObject iteractAction;

    private MobileController mobileController = null;

    void Start() {
        if (GameObject.FindGameObjectWithTag("MobileGlobal") != null)
            mobileController = GameObject.FindGameObjectWithTag("MobileGlobal").GetComponent<MobileController>();

        uiIcon.transform.GetChild(0).GetComponent<TextMeshPro>().text = Language.getInstance().get(hintStringKey);
        uiIcon.transform.GetChild(1).GetComponent<TextMeshPro>().text = iteractKey;
        uiIcon.SetActive(false);
    }

    public void activateAction(GameObject executor) {
        if (iteractAction.GetComponent<DialogAction>() != null)
            iteractAction.GetComponent<DialogAction>().activate(executor);

        if (iteractAction.GetComponent<DoorAction>() != null)
            iteractAction.GetComponent<DoorAction>().activate();

        if (iteractAction.GetComponent<BoxAction>() != null)
            iteractAction.GetComponent<BoxAction>().activate();

        if (iteractAction.GetComponent<MoveToAction>() != null)
            iteractAction.GetComponent<MoveToAction>().activate(executor);

        if (iteractAction.GetComponent<FireArms>() != null)
            iteractAction.GetComponent<FireArms>().onGetWeapon(executor);

        if (iteractAction.GetComponent<BoxArmorAction>() != null)
            iteractAction.GetComponent<BoxArmorAction>().activate(executor);


        if (onActivationDestroy) Destroy(gameObject);
    }

    public void onViewedAction(bool isView) {
        if (isTrigger) return;
        if (Constants.isMobile && mobileController != null) {
            mobileController.onShowHideActionUI(isView, Language.getInstance().get(hintStringKey));
        } else {
            uiIcon.SetActive(isView);
        }
    }

    void OnTriggerEnter2D(Collider2D col) {
        if (col.gameObject.transform.parent.GetComponent<CharecterController>() == null) return;
        if (isTrigger) {
            activateAction(col.gameObject.transform.parent.gameObject);
        } else {
            col.gameObject.transform.parent.GetComponent<CharecterController>().availableAction(gameObject);
        }
    }

    void OnTriggerExit2D(Collider2D col) {
        if (col.gameObject.transform.parent.GetComponent<CharecterController>() == null) return;

        if (col.gameObject.transform.parent.GetComponent<CharecterController>().getAvailableAction() == gameObject)
            col.gameObject.transform.parent.GetComponent<CharecterController>().availableAction(null);
    }
}
