﻿using UnityEngine;

public class FloorController : MonoBehaviour {
    void OnTriggerEnter2D(Collider2D col) {
        if (col.gameObject.transform.parent.GetComponent<CharecterController>() != null) {
            col.gameObject.transform.parent.GetComponent<CharecterController>().setLocationYPosition(transform.position.y);
        }
    }
}
